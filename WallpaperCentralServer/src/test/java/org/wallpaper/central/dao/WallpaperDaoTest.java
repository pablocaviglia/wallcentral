package org.wallpaper.central.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wallpaper.central.filter.WallpaperFilter;
import org.wallpaper.central.model.Wallpaper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:application-context.xml"})
public class WallpaperDaoTest {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private WallpaperDao wallpaperDao;
	
	@Test
	public void getCategoriesTest(){
		List<String> categories = wallpaperDao.getCategories();
		logger.info("The categories are: ");
		for (String category : categories) {
			logger.info("   " + category);
		}
	}
	
	@Test
	public void findWallpapersTest(){
		//filtro de busqueda
		WallpaperFilter filter = new WallpaperFilter();
		filter.setFirstResult(0);
		filter.setMaxResults(10);
		filter.setCategory("Architecture");
		filter.setPopular(true);
		
		int total = wallpaperDao.countFindWallpapers(filter);
		logger.info("The total is: " + total);
		List<Wallpaper> wallpapers = new ArrayList<Wallpaper>();
		if(total > 0) {
			wallpapers = wallpaperDao.findWallpapers(filter);	
		}
		
		logger.info("The first page results are: ");
		for (Wallpaper wallpaper : wallpapers) {
			logger.info("Category: "+ wallpaper.getCategory() + ", Downloads: "+ wallpaper.getDownloads() + ", Name: " + wallpaper.getName());
		}
	}

}
