package org.wallpaper.central.model;

import java.util.List;

public class ScrapConfig {

	//cantidad de elementos por pagina
	private int elementsPerPage;
	private String thumbnailFolder;
	
	//lista de categorias
	private List<Category> categories;

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public int getElementsPerPage() {
		return elementsPerPage;
	}

	public void setElementsPerPage(int elementsPerPage) {
		this.elementsPerPage = elementsPerPage;
	}

	public String getThumbnailFolder() {
		return thumbnailFolder;
	}

	public void setThumbnailFolder(String thumbnailFolder) {
		this.thumbnailFolder = thumbnailFolder;
	}
}