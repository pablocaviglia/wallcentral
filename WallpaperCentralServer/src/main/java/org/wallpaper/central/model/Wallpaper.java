package org.wallpaper.central.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "WALLPAPER")
public class Wallpaper implements Serializable {

	private static final long serialVersionUID = -5861379957815949584L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Column(name = "DESKTOP_NEXUS_ID")
	private Long desktopNexusId;

	@Column(name = "URL", unique = true, nullable = false)
	private String url;

	@Column(name = "STATUS", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private WallpaperStatus status;

	@Index(name="categoryIndex")
	@Column(name = "CATEGORY", nullable = false)
	private String category;

	@Column(name = "KEYWORDS", nullable = true, length=512)
	@Index(name="wallpaperIndex")
	private String keywords;

	@Column(name = "SUBCATEGORY")
	private String subcategory;

	@Column(name = "NAME", nullable = false)
	@Index(name="wallpaperNameIndex")
	private String name;

	@Column(name = "WIDTH")
	private Integer width;
	
	@Column(name = "HEIGHT")
	private Integer height;

	@Column(name = "DOWNLOADS")
	private Integer downloads;

	@Column(name = "FILE_SIZE")
	private Long fileSize;

	@Column(name = "TIMES_FAVORITED")
	private Integer timesFavorited;
	
	@Column(name = "FOLDER", nullable = true)
	private String folder;

	@Column(name = "FILENAME", nullable = true, unique = true)
	private String filename;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WallpaperStatus getStatus() {
		return status;
	}

	public void setStatus(WallpaperStatus status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getDownloads() {
		return downloads;
	}

	public void setDownloads(Integer downloads) {
		this.downloads = downloads;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public Integer getTimesFavorited() {
		return timesFavorited;
	}

	public void setTimesFavorited(Integer timesFavorited) {
		this.timesFavorited = timesFavorited;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	public Long getDesktopNexusId() {
		return desktopNexusId;
	}

	public void setDesktopNexusId(Long desktopNexusId) {
		this.desktopNexusId = desktopNexusId;
	}
	
	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String toString() {
		return id != null ? id.toString() : "-1";
	}
}