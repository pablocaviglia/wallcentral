package org.wallpaper.central.model;

public class ApplicationConfig {

	public String initialSearch;
	public String shareWallpaperURL;
	public int searchPageSize;
	
}
