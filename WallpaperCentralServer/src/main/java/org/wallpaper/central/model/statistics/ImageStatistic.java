package org.wallpaper.central.model.statistics;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author mariano
 *
 */
@Entity
@Table(name="IMAGE_STATISTICS")
public class ImageStatistic {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private long id;
	
	@Column(name="TIMESTAMP", nullable=false)
	private long timestamp;
	
	@Column(name="DURATION", nullable=false)
	private int duration;
	
	@Column(name="WALLPAPER_ID", nullable=false)
	private long wallpaperId;
	
	@Column(name="HEIGHT", nullable=false)
	private int height;
	
	@Column(name="WIDTH", nullable=false)
	private int width;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public long getWallpaperId() {
		return wallpaperId;
	}

	public void setWallpaperId(long wallpaperId) {
		this.wallpaperId = wallpaperId;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	
}
