package org.wallpaper.central.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.wallpaper.central.service.ImageService;

/**
 * Servicios REST para descargar imagenes.
 * 
 * @author mesteves
 */
@Controller
@RequestMapping(value="/image")
public class ImageController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

	@Autowired
	private ImageService imageService;

	@RequestMapping(value="/get/{wallpaperId}/{width}/{height}", produces = MediaType.IMAGE_JPEG_VALUE, method = RequestMethod.GET)
	public @ResponseBody
	byte[] getImage(@PathVariable long wallpaperId, 
					@PathVariable int width,
						@PathVariable int height) throws Exception {
		LOGGER.debug(" : " + wallpaperId + " - " + width + "x" + height);
		return imageService.getImage(wallpaperId, width, height);
	}
	
}