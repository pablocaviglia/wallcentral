package org.wallpaper.central.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.wallpaper.central.config.ApplicationConfigReader;
import org.wallpaper.central.dto.LoginResult;
import org.wallpaper.central.dto.SearchResult;
import org.wallpaper.central.dto.WallpaperDTO;
import org.wallpaper.central.dto.WallpaperInfoDTO;
import org.wallpaper.central.filter.WallpaperFilter;
import org.wallpaper.central.model.Wallpaper;
import org.wallpaper.central.service.WallpaperService;

@Controller
@RequestMapping(value="/wallpaper")
public class WallpaperController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired 
	private ApplicationConfigReader applicationConfigReader;
	
	@Autowired
	private WallpaperService wallpaperService;
	
	@RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public LoginResult<WallpaperDTO> login(HttpServletRequest request, @RequestParam(required=false) String category) {
		
		logger.debug("Logging in to WallCentral...");
		logger.debug("User IP: " + request.getRemoteAddr());
		
		//create the result object
		LoginResult<WallpaperDTO> loginResult = new LoginResult<WallpaperDTO>();
		loginResult.setLoginTimestamp(new Timestamp(System.currentTimeMillis()));
		
		if(category == null) {
			//obtenemos la lista de categorias
			List<String> categories = wallpaperService.getCategories();
			
			//elegimos una categoria por defecto
			String randomCategory = categories.get(new Random().nextInt(categories.size()));
			
			SearchResult<WallpaperDTO> defaultSearch = search(0, null, randomCategory, null, true);
			loginResult.setDefaultSearch(randomCategory);
			loginResult.setDefaultSearchResult(defaultSearch);
			loginResult.setCategories(categories);
		}
		else {
			//obtenemos la lista de subcategorias
			List<String> subcategories = wallpaperService.getSubcategories(category);
			
			//elegimos una categoria por defecto
			String randomSubcategory = subcategories.get(new Random().nextInt(subcategories.size()));
			
			SearchResult<WallpaperDTO> defaultSearch = search(0, null, category, randomSubcategory, true);
			loginResult.setDefaultSearch(randomSubcategory);
			loginResult.setDefaultSearchResult(defaultSearch);
			loginResult.setCategories(subcategories);
		}
		
		return loginResult;
	}
	
	@RequestMapping(value = "/search", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public SearchResult<WallpaperDTO> search(@RequestParam(required=false) Integer offset,
			@RequestParam(required=false) String keywords,
			@RequestParam(required=false) String category,
			@RequestParam(required=false) String subcategory,
			@RequestParam(required=false) Boolean popular) {
		
		logger.debug("Getting wallpapers...  "
				+ "pageNumber: " + offset + ", "
				+ "keyword: " + keywords + ", "
				+ "category: " + category + ", "
				+ "subcategory: " + subcategory + ", "
				+ "popular: " + popular);
		
		//filtro de busqueda
		WallpaperFilter filter = new WallpaperFilter();
		filter.setFirstResult(offset);
		filter.setMaxResults(applicationConfigReader.getApplicationConfig().searchPageSize);
		filter.setKeywords((keywords != null && !keywords.trim().equals("")) ? keywords : null );
		filter.setCategory((category != null && !category.trim().equals("")) ? category : null );
		filter.setSubcategory((subcategory != null && !subcategory.trim().equals("")) ? subcategory : null );
		filter.setPopular(popular);
		
		int total = wallpaperService.countFindWallpapers(filter);
		List<Wallpaper> wallpapers = new ArrayList<Wallpaper>();
		if(total > 0) {
			wallpapers = wallpaperService.findWallpapers(filter);	
		}
		
		//creamos resultado a devolver
		SearchResult<WallpaperDTO> result = new SearchResult<WallpaperDTO>();
		result.setTotal(total);
		for(Wallpaper wallpaper : wallpapers) {
			result.getElements().add(new WallpaperDTO(wallpaper));	
		}
		
		return result;
	}
	
	@RequestMapping(value = "/categories", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public List<String> getCategories() {
		logger.debug("Get categories ...");
		return wallpaperService.getCategories();
	}
	
	@RequestMapping(value = "/subcategories", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public List<String> getSubcategories(@RequestParam(required=false) String category) {
		logger.debug("Get '" + category + "' subcategories ...");
		return wallpaperService.getSubcategories(category);
	}
	
	@RequestMapping(value = "/info", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public WallpaperInfoDTO info(@RequestParam(required=true) Long wallpaperId) {
		logger.debug("Get info wallpaper " + wallpaperId + " ...");
		return new WallpaperInfoDTO(wallpaperService.findById(wallpaperId));
	}
}