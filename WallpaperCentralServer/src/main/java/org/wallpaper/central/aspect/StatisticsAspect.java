package org.wallpaper.central.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.wallpaper.central.model.statistics.ImageStatistic;
import org.wallpaper.central.service.ImageStatisticService;

/**
 * Aspecto que se encarga de almacenar los datos de estadísticas de las imagenes.
 */
@Aspect
public class StatisticsAspect {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private ImageStatisticService imageStatisticService;


    @Around(value = "execution(byte[] org.wallpaper.central.controller.ImageController.getImage(..))")
    public Object saveStatistics(final ProceedingJoinPoint proceedingJoinPoint) throws Exception {
    	 long startTime;
    	 int duration;
         Object output = null;
         /* ** BEFORE ** */
         startTime = System.currentTimeMillis();
    	try {
            output = proceedingJoinPoint.proceed();
        } catch (Throwable t) { // NOPMD
            logger.error(t.getMessage(), t);
        } finally {
            /* ** AFTER ** */
            duration = (int)(System.currentTimeMillis() - startTime);
            logger.debug("End method " + proceedingJoinPoint.toString() + ", duration: {}", duration);
            
            ImageStatistic imageStatistic = new ImageStatistic();
            imageStatistic.setDuration(duration);
            imageStatistic.setTimestamp(startTime);
            imageStatistic.setWallpaperId((Long)proceedingJoinPoint.getArgs()[0]);
            imageStatistic.setWidth((Integer)proceedingJoinPoint.getArgs()[1]);
            imageStatistic.setHeight((Integer)proceedingJoinPoint.getArgs()[2]);
            saveImageStatistic(imageStatistic);
        }
    	
    	return output;
    }
    
    @Async
    private void saveImageStatistic(ImageStatistic imageStatistic){
    	imageStatisticService.store(imageStatistic);
    }
}
