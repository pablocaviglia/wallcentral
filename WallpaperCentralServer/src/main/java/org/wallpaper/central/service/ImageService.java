package org.wallpaper.central.service;

public interface ImageService {

	byte[] getImage(long wallpaperId, int width, int height) throws Exception;

}