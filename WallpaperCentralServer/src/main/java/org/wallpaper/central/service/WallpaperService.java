package org.wallpaper.central.service;

import java.util.List;

import org.wallpaper.central.filter.WallpaperFilter;
import org.wallpaper.central.model.Wallpaper;

public interface WallpaperService {

	Wallpaper findById(long wallpaperId);

	List<Wallpaper> findWallpapers(WallpaperFilter filter);

	int countFindWallpapers(WallpaperFilter filter);

	List<String> getCategories();
	
	List<String> getSubcategories(String category);

}