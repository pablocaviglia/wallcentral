package org.wallpaper.central.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.gm4java.engine.GMConnection;
import org.gm4java.engine.GMException;
import org.gm4java.engine.GMServiceException;
import org.gm4java.engine.support.PooledGMService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wallpaper.central.dao.WallpaperDao;
import org.wallpaper.central.model.Wallpaper;

import com.googlecode.ehcache.annotations.Cacheable;

@Service
public class ImageServiceImpl implements ImageService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private PooledGMService gmService;

	@Autowired
	private WallpaperDao wallpaperDao;

	/* (non-Javadoc)
	 * @see org.wallpaper.central.service.ImageService#getImage(long, int, int)
	 */
	@Cacheable(cacheName="wallpaperImagesCache")
	@Override
	public byte[] getImage(long wallpaperId, int width, int height) throws Exception {
		logger.debug("getImage : " + wallpaperId + " - " + width + "x" + height);
		try {

			File f = getScaledImage(wallpaperId, width, height);
			FileInputStream fis = new FileInputStream(f);
			byte[] result = IOUtils.toByteArray(fis);
			boolean deleted = f.delete();
			if (!deleted) {
				logger.warn("The temp file " + f.getAbsolutePath()
						+ " wasn't be deleted");
			}
			return result;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private File getScaledImage(long wallpaperId, int width, int height)
			throws IOException, InterruptedException {
		File imgResized = File.createTempFile(
				"resizedImg_" + System.currentTimeMillis() + "_", ".jpg");
		try {
			GMConnection gmConnection = gmService.getConnection();
			Wallpaper wallpaper = wallpaperDao.findById(wallpaperId);
			String imgPath = String.format("/walls/%s/%s",
					wallpaper.getFolder(), wallpaper.getFilename());

			gmConnection.execute(String.format(
					"convert -quality 95 %s -resize %dx%d %s", imgPath, width,
					height, imgResized.getAbsolutePath()));
			gmConnection.close();
		} catch (GMServiceException e) {
			logger.error(e.getMessage(), e);
		} catch (GMException e) {
			logger.error(e.getMessage(), e);
		}
		return imgResized;
	}

}
