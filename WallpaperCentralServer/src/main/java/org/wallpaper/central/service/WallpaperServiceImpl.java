package org.wallpaper.central.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wallpaper.central.dao.WallpaperDao;
import org.wallpaper.central.filter.WallpaperFilter;
import org.wallpaper.central.model.Wallpaper;

import com.googlecode.ehcache.annotations.Cacheable;

@Service
public class WallpaperServiceImpl implements WallpaperService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private WallpaperDao wallpaperDao;
	
	@Override
	@Cacheable(cacheName="wallpaperInfoCache")
	@Transactional(readOnly = true)
	public Wallpaper findById(long wallpaperId) {
		logger.debug("Find wallpaper by id");
		return wallpaperDao.findById(wallpaperId);
	}
	
	@Override
	@Cacheable(cacheName="wallpaperSearchCache")
	@Transactional(readOnly = true)
	public List<Wallpaper> findWallpapers(WallpaperFilter filter) {
		logger.debug("Find wallpapers");
		return wallpaperDao.findWallpapers(filter);
	}
	
	@Override
	@Cacheable(cacheName="countWallpaperSearchCache")
	@Transactional(readOnly = true)
	public int countFindWallpapers(WallpaperFilter filter) {
		logger.debug("Count find wallpapers");
		return wallpaperDao.countFindWallpapers(filter);
	}
	
	@Override
	@Cacheable(cacheName="categoriesCache")
	@Transactional(readOnly = true)
	public List<String> getCategories() {
		logger.debug("Get categories");
		return wallpaperDao.getCategories();
	}

	@Override
	@Cacheable(cacheName="subcategoriesCache")
	@Transactional(readOnly = true)
	public List<String> getSubcategories(String category) {
		logger.debug("Getting '" + category + "' subcategories");
		return wallpaperDao.getSubcategories(category);
	}
}