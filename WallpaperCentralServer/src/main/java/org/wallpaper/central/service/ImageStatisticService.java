package org.wallpaper.central.service;

import org.wallpaper.central.model.statistics.ImageStatistic;

public interface ImageStatisticService {

	void store(ImageStatistic imageStatistic);
	
}