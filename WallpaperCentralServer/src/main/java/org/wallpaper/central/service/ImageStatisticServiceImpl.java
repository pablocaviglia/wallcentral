package org.wallpaper.central.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.wallpaper.central.dao.ImageStatisticDao;
import org.wallpaper.central.model.statistics.ImageStatistic;

@Service
public class ImageStatisticServiceImpl implements ImageStatisticService {

	@Autowired
	private ImageStatisticDao imageStatisticDao;

	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly=false)
	public void store(ImageStatistic imageStatistic) {
		imageStatisticDao.store(imageStatistic);
	}

}
