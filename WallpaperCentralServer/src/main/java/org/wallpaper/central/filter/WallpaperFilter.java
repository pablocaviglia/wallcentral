package org.wallpaper.central.filter;

import org.wallpaper.central.model.WallpaperStatus;

public class WallpaperFilter {
	
	private WallpaperStatus status;
	private String category;
	private String subcategory;
	private String name;
	private String keywords;
	private Boolean popular; 

	/**
	 * firstResult Indicates the index of the firstResult
	 */
	private Integer firstResult;

	/**
	 * maxResult Indicates the maximun number of result to retrieve in a query
	 */
	private Integer maxResults;

	public WallpaperStatus getStatus() {
		return status;
	}

	public void setStatus(WallpaperStatus status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Boolean getPopular() {
		return popular;
	}

	public void setPopular(Boolean popular) {
		this.popular = popular;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
}