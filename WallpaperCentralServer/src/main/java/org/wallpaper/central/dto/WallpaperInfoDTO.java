package org.wallpaper.central.dto;

import org.wallpaper.central.model.Wallpaper;

public class WallpaperInfoDTO extends WallpaperDTO {
	private static final long serialVersionUID = -5978424121353444290L;

	private int downloads;
	private long fileSize;
	private int width;
	private int height;
	private String keywords;
	private String category;

	public WallpaperInfoDTO(Wallpaper wallpaper) {
		super(wallpaper);
		downloads = wallpaper.getDownloads();
		fileSize = wallpaper.getFileSize();
		width = wallpaper.getWidth();
		height = wallpaper.getHeight();
		keywords = wallpaper.getKeywords();
		category = wallpaper.getCategory();
	}

	public int getDownloads() {
		return downloads;
	}

	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}