package org.wallpaper.central.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class LoginResult<T extends Serializable> implements Serializable {

	private static final long serialVersionUID = 7155178130049181834L;
	
	private Timestamp loginTimestamp;
	private String defaultSearch;
	private SearchResult<T> defaultSearchResult;
	private List<String> categories = new ArrayList<String>();
	
	public Timestamp getLoginTimestamp() {
		return loginTimestamp;
	}
	public void setLoginTimestamp(Timestamp loginTimestamp) {
		this.loginTimestamp = loginTimestamp;
	}
	public SearchResult<T> getDefaultSearchResult() {
		return defaultSearchResult;
	}
	public void setDefaultSearchResult(
			SearchResult<T> defaultSearchResult) {
		this.defaultSearchResult = defaultSearchResult;
	}
	public List<String> getCategories() {
		return categories;
	}
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}
	public String getDefaultSearch() {
		return defaultSearch;
	}
	public void setDefaultSearch(String defaultSearch) {
		this.defaultSearch = defaultSearch;
	}
}