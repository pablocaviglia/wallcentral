package org.wallpaper.central.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchResult<E extends Serializable> implements Serializable {

	private static final long serialVersionUID = -4124071800475619053L;

	private int total;
	private List<E> elements = new ArrayList<E>();

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<E> getElements() {
		return elements;
	}

	public void setElements(List<E> elements) {
		this.elements = elements;
	}
}
