package org.wallpaper.central.dto;

import java.io.Serializable;

public class SearchFilter implements Serializable {
	
	private static final long serialVersionUID = -6921318702673066612L;
	
	private int pageNumber;
	private String keyword;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
