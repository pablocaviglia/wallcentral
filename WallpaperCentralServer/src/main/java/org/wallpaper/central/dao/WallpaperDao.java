package org.wallpaper.central.dao;

import java.util.List;

import org.wallpaper.central.filter.WallpaperFilter;
import org.wallpaper.central.model.Wallpaper;

public interface WallpaperDao extends GenericDao<Wallpaper>{

	List<Wallpaper> findWallpapers(WallpaperFilter filter);

	int countFindWallpapers(WallpaperFilter filter);

	List<String> getCategories();
	
	List<String> getSubcategories(String category);

}