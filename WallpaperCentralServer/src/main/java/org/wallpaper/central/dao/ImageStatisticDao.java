package org.wallpaper.central.dao;

import org.wallpaper.central.model.statistics.ImageStatistic;

public interface ImageStatisticDao extends GenericDao<ImageStatistic>{

}