package org.wallpaper.central.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.wallpaper.central.filter.WallpaperFilter;
import org.wallpaper.central.model.Wallpaper;

@Repository
public class WallpaperDaoImpl extends GenericDaoImpl<Wallpaper> implements WallpaperDao{
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Wallpaper> findWallpapers(WallpaperFilter filter) {
		logger.debug("Find wallpapers");
		Query query = buildQueryfindWallpapers(filter, false);
		if (filter.getFirstResult() != null) {
			query.setFirstResult(filter.getFirstResult());
		}
		if (filter.getMaxResults() != null) {
			query.setMaxResults(filter.getMaxResults());
		}

		return query.getResultList();
	}
	
	@Override
	public int countFindWallpapers(WallpaperFilter filter) {
		logger.debug("Count find wallpapers");
		Query query = buildQueryfindWallpapers(filter, true);
		long result = (Long) query.getSingleResult();
		return (int) result;
	}
	
	/**
	 * This method build the findWallpaper query.
	 * 
	 * @param filter
	 *            Is a pojo with the fields to filter the Wallpapers
	 * @param count
	 *            Is a boolean to indicate if the query should return the count,
	 *            or the elements
	 * @return Query
	 */
	private Query buildQueryfindWallpapers(WallpaperFilter filter, boolean count) {
		StringBuilder sb = new StringBuilder();
		if (filter.getStatus() != null) {
			sb.append(" where w.status = :status");
		}
		if (filter.getCategory() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.category = :category");
		}
		if (filter.getSubcategory() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.subcategory = :subcategory");
		}
		
		if (filter.getName() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.name = :name ");
		}
		
		if (filter.getKeywords() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.keywords like '%" + filter.getKeywords() + "%'");
		}
		
		if (filter.getPopular() != null && filter.getPopular()) {
			sb.append(" order by w.downloads desc");
		}

		sb.insert(0, " from Wallpaper w ");
		
		if (count) {
			sb.insert(0, " select count(w) as qntyWallpapers ");
		}

		Query query = entityManager.createQuery(sb.toString());
		
		if (filter.getStatus() != null) {
			query.setParameter("status", filter.getStatus());
		}
		if (filter.getCategory() != null) {
			query.setParameter("category", filter.getCategory());
		}
		if (filter.getSubcategory() != null) {
			query.setParameter("subcategory", filter.getSubcategory());
		}
		if (filter.getName() != null) {
			query.setParameter("name", filter.getName());
		}
		return query;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> getCategories() {
		logger.debug("Get categories");
		Query query = entityManager.createQuery("select distinct(w.category) from Wallpaper w");
		return  (List<String>) query.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<String> getSubcategories(String category) {
		logger.debug("Get categories");
		Query query = entityManager.createQuery("select distinct(w.subcategory) from Wallpaper w where w.category=:category");
		query.setParameter("category", category);
		return  (List<String>) query.getResultList();
	}
}