package org.wallpaper.central.dao;

import org.springframework.stereotype.Repository;
import org.wallpaper.central.model.statistics.ImageStatistic;

@Repository
public class ImageStatisticDaoImpl extends GenericDaoImpl<ImageStatistic> implements ImageStatisticDao{

}