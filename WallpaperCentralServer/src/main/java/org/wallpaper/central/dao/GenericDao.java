package org.wallpaper.central.dao;

import java.util.List;

public interface GenericDao<T> {

	void store(T t);

	void delete(Long id);

	T findById(Long id);

	List<T> findAll();

	void flush();

}