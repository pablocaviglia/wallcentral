package org.wallpaper.central.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public class GenericDaoImpl<T> implements GenericDao<T> {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> type;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        if (t instanceof ParameterizedType) {
	        ParameterizedType pt = (ParameterizedType) t;
	        type = (Class) pt.getActualTypeArguments()[0];
        } else {
        	type = (Class) t;
        }
    }

	/* (non-Javadoc)
	 * @see org.wallpaper.central.dao.GenericDao#store(T)
	 */
	@Override
	@Transactional
	public void store(T t) {
		logger.debug("Storing " + type.getSimpleName() + " " + t);
		entityManager.merge(t);
	}
	
	/* (non-Javadoc)
	 * @see org.wallpaper.central.dao.GenericDao#delete(java.lang.Long)
	 */
	@Override
	@Transactional
	public void delete(Long id) {
		logger.debug("Deleting " + type.getSimpleName() + " " + id);
		entityManager.remove(entityManager.find(type, id));
	}

	/* (non-Javadoc)
	 * @see org.wallpaper.central.dao.GenericDao#findById(java.lang.Long)
	 */
	@Override
	@Transactional(readOnly = true)
	public T findById(Long id) {
		logger.debug("Find  " + type.getSimpleName() + " " + id);
		return entityManager.find(type, id);
	}

	/* (non-Javadoc)
	 * @see org.wallpaper.central.dao.GenericDao#findAll()
	 */
	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findAll() {
		logger.debug("Find all " + type.getSimpleName());
		Query query = entityManager.createQuery("from " + type.getSimpleName());
		return query.getResultList();
	}
	
	/* (non-Javadoc)
	 * @see org.wallpaper.central.dao.GenericDao#flush()
	 */
	@Override
	public void flush() {
		entityManager.flush();
	}
}
