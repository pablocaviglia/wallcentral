package com.gmi.wallcentralclientanime;
import com.gmi.wallcentralclientbase.App;

public class AnimeApp extends App {
	
	@Override
	public void initialize() {
		App.CATEGORIA_BASE = "anime";
		App.ADMOB_BANNER_ID = "ca-app-pub-6328885496949159/3503624627";
		App.ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-6328885496949159/4980357829";
	}
}