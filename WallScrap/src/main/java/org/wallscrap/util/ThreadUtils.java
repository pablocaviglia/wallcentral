package org.wallscrap.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ThreadUtils {
	private static final Log logger = LogFactory.getLog(ThreadUtils.class);

	public static void sleep(long milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
