package org.wallscrap.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class ShellClient {

	public static  String exec(String[] commands) throws IOException, InterruptedException {

		for(String command: commands) {
			System.out.print(command);
		}
		System.out.println();
		
		String ret = "";

		final Process proc = Runtime.getRuntime().exec("/bin/bash", null, new File("/bin"));

		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())), true);
		for(int i=0; i<commands.length; i++) {
			out.println(commands[i]);
		}
		out.println("exit");
		out.close();

		try {
			
			Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						String line = "";
						do {
							System.out.println(line);
						}
						while ((line = in.readLine()) != null);
						in.close();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			Thread t2 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader inError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
						String line = "";
						do {
							System.out.println(line);
						}
						while ((line = inError.readLine()) != null);
						inError.close();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			t1.start();
			t2.start();
			
			t1.join();
			t2.join();
			
			proc.waitFor();
			proc.destroy();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}

		return ret;
	}
	
	public static void main(String[] args) {
		
		try {
			
			//thumbnail imagen
			ShellClient.exec(new String[]{"convert -thumbnail 160 /home/mesteves/Desktop/s5mskvmbdq22mnuibfqcucpcq55241e0270a9fa.jpeg /home/mesteves/Desktop/s5mskvmbdq22mnuibfqcucpcq55241e0270a9fa.thumb.jpeg"});
			
			//cambiar exif
//			ShellClient.exec(new String[]{"exiftool -comment='desde java comment' -copyright='desde java copyright' /home/mesteves/Desktop/s5mskvmbdq22mnuibfqcucpcq55241e0270a9fa.jpeg"});
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}