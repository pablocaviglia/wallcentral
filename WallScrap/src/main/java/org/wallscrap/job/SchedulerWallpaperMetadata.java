package org.wallscrap.job;

import java.util.List;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.wallscrap.business.WallpaperMetadataScraper;
import org.wallscrap.business.constant.WallpaperScrapStatus;
import org.wallscrap.config.XMLScrapConfigReader;
import org.wallscrap.dao.WallpaperDao;
import org.wallscrap.filter.WallpaperFilter;
import org.wallscrap.iface.MetadataDocumentListener;
import org.wallscrap.model.Category;
import org.wallscrap.model.Wallpaper;

@Service
public class SchedulerWallpaperMetadata implements MetadataDocumentListener {

	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private WallpaperDao wallpaperDao;
	
	@Autowired
	private WallpaperMetadataScraper wallpaperMetadataScraper;
	
	@Autowired
	private XMLScrapConfigReader xmlScrapConfigReader;
	
	private Stack<Object[]> processingStack;
	private ThreadPoolExecutor executorPool;
	
	private int processingElements;
	
	public SchedulerWallpaperMetadata() {
		super();
		processingStack = new Stack<Object[]>();
		executorPool = new ThreadPoolExecutor(3, 6, 3, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(200));
	}
	
//	@PostConstruct
	public void init() {
		logger.debug("Cambiamos de estado '" + WallpaperScrapStatus.PROCESSING_METADATA + "' a ' " + WallpaperScrapStatus.FIRST_REGISTRATION + "'...");
		int modRegs = wallpaperDao.cambiarEstado(WallpaperScrapStatus.PROCESSING_METADATA, WallpaperScrapStatus.FIRST_REGISTRATION);
		logger.debug(modRegs + " wallpapers vueltos a " + WallpaperScrapStatus.FIRST_REGISTRATION + "!");
	}

	@Scheduled(fixedRate=2000)
	private void recollectWallpaperDocuments() {
		
		List<Category> categories = xmlScrapConfigReader.getScrapConfig().getCategories();
		for (Category category : categories) {

			WallpaperFilter filter = new WallpaperFilter();
			filter.setStatus(WallpaperScrapStatus.FIRST_REGISTRATION);
			filter.setCategory(category.getName());
			filter.setMaxResults(40);
			List<Wallpaper> wallpapers = wallpaperDao.findWallpapers(filter);
			for(Wallpaper wallpaper : wallpapers) {
				
//				//no dejamos que se tengan en memoria mas
//				//de X documentos html al mismo tiempo
//				if(processingElements < 20) {
					wallpaper.setStatus(WallpaperScrapStatus.PROCESSING_METADATA);
					wallpaperDao.store(wallpaper);
					processingElements++;
					executorPool.execute(new WorkerWallpaperMetadataThread(wallpaper, wallpaperMetadataScraper, this));
//				}
			}
		}
	}
	
	@Scheduled(fixedRate=5000)
	private void processWallpaperMetadata() {
		
		if(processingStack.size() > 0) {
			while(processingStack.size() > 0) {
				
				Object[] toProcess = processingStack.pop();
				Wallpaper wallpaper = (Wallpaper)toProcess[0];
				Document wallpaperDocument = (Document)toProcess[1];
				
				//scrapeamos wallpaper
				wallpaperMetadataScraper.scrapWallpaperMetadata(wallpaper, wallpaperDocument);
				
			}
		}
	}
	
	@Override
	public void metadataDocumentDownloaded(Wallpaper wallpaper, Document wallpaperDocument, EXECUTION_RESULTS executionResult) {
		
		if(executionResult == EXECUTION_RESULTS.OK) {
			processingStack.push(new Object[]{wallpaper, wallpaperDocument});
		}
		else if(executionResult == EXECUTION_RESULTS.DESKTOP_NEXUS_INVALID) {
			//wallpaper invalido en desktop 
			//nexus, invalidamos localmente
			wallpaper.setStatus(WallpaperScrapStatus.INVALID_ON_DESKTOP_NEXUS);
			wallpaperDao.store(wallpaper);
		}
		else if(executionResult == EXECUTION_RESULTS.EXCEPTION) {
			//excepcion, volvemos a estado anterior
			wallpaper.setStatus(WallpaperScrapStatus.FIRST_REGISTRATION);
			wallpaperDao.store(wallpaper);
		}
		
		decrementProcessingElements();
	}
	
	private synchronized void decrementProcessingElements() {
		processingElements--;
	}
}