package org.wallscrap.job;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.wallscrap.exception.ScrappingException;
import org.wallscrap.iface.ImageDownloadListener;
import org.wallscrap.iface.ImageDownloadListener.EXECUTION_RESULTS;
import org.wallscrap.model.Wallpaper;
import org.wallscrap.util.SizeInputStream;

@Component
@Scope("prototype")
public class WorkerWallpaperImageThread implements Runnable {

	private final Log logger = LogFactory.getLog(getClass());
	
	private Wallpaper wallpaper;
	private ImageDownloadListener listener;
	
	public WorkerWallpaperImageThread(Wallpaper wallpaper, ImageDownloadListener listener) {
		super();
		this.wallpaper = wallpaper;
		this.listener = listener;
	}
	
	@Override
	public void run() {
		
		try {
			
			//bajamos la imagen
			byte[] imageData = scrapWallpaperImage(wallpaper);
			
			//llamamos al listener
			listener.imageDownloaded(wallpaper, imageData, EXECUTION_RESULTS.OK);
			
		}
		catch (FileNotFoundException e) {
			logger.error(e.getMessage() + " --- Not exist on Desktop Nexus");
			listener.imageDownloaded(wallpaper, null, EXECUTION_RESULTS.INVALID_ON_DESKTOP_NEXUS);
		}
		catch (Exception e) {
			logger.error(e.getMessage() + " --- IO Error while downloading image for Wallpaper ID: " + wallpaper.getId());
			listener.imageDownloaded(wallpaper, null, EXECUTION_RESULTS.BAD);
		}
	}
	
	/**
	 * Scrap the wallpaper image
	 * 
	 * @param wallpaper
	 * @throws IOException
	 * @throws ScrappingException 
	 */
	private byte[] scrapWallpaperImage(Wallpaper wallpaper) throws IOException, ScrappingException {
		
		//obtenemos las cookies
		Map<String, String> cookies = SchedulerWallpaperImage.getCookies();

		String wallpaperImageDataUrl = "http://www.desktopnexus.com/dl/" + wallpaper.getDesktopNexusId() + "/" + wallpaper.getWidth() + "x" + wallpaper.getHeight() + "/" + cookies.get("PHPSESSID");
		logger.debug("Scrapping wallpaper "+ wallpaperImageDataUrl);
		
		//parseamos las cookies para
		//postearlas en el request a la imagen
		String cookiesParsed = "";
		Iterator<String> cookieKeysIt = cookies.keySet().iterator();
		while(cookieKeysIt.hasNext()) {
			String cookieKey = cookieKeysIt.next();
			String cookieValue = cookies.get(cookieKey);
			cookiesParsed += cookieKey + "=" + cookieValue + ";"; 
		}
		
		if(cookies.size() > 0) {
			cookiesParsed = cookiesParsed.substring(0, cookiesParsed.length()-1);
		}
		
		URL myUrl = new URL(wallpaperImageDataUrl);
		URLConnection urlConn = myUrl.openConnection();
		urlConn.setRequestProperty("Cookie", cookiesParsed);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		SizeInputStream bis = new SizeInputStream(new BufferedInputStream(urlConn.getInputStream()));
		
		boolean error = false;
		
		byte[] buffer = new byte[8192];
		int i = -1;
		while((i = bis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
			if(new String(buffer).indexOf("DOCTYPE") != -1) {
				error = true;
				break;
			}
		}
		baos.flush();
		baos.close();
		bis.close();
		
		if(error) {
			return null;
		}
		else {
			return baos.toByteArray();	
		}
	}
}