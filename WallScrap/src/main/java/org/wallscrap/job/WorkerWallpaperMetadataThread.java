package org.wallscrap.job;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.wallscrap.business.WallpaperMetadataScraper;
import org.wallscrap.iface.MetadataDocumentListener;
import org.wallscrap.iface.MetadataDocumentListener.EXECUTION_RESULTS;
import org.wallscrap.model.Wallpaper;

@Component
@Scope("prototype")
public class WorkerWallpaperMetadataThread implements Runnable {

	private final Log logger = LogFactory.getLog(getClass());

	private Wallpaper wallpaper;
	private WallpaperMetadataScraper wallpaperMetadataScraper;
	private MetadataDocumentListener listener;
	
	public WorkerWallpaperMetadataThread(Wallpaper wallpaper, WallpaperMetadataScraper wallpaperMetadataScraper, MetadataDocumentListener listener) {
		super();
		this.wallpaper = wallpaper;
		this.wallpaperMetadataScraper = wallpaperMetadataScraper;
		this.listener = listener;
	}

	@Override
	public void run() {
		try {

			logger.debug("Retrieving HTML document for wallpaper id: " + wallpaper.getId() + " (" + wallpaper.getUrl() + ")");
			
			//obtenemos el documento desde la web
			Document wallpaperDocument = wallpaperMetadataScraper.requestWallpaperMetadataDOM(wallpaper);
			listener.metadataDocumentDownloaded(wallpaper, wallpaperDocument, wallpaperDocument != null ? EXECUTION_RESULTS.OK : EXECUTION_RESULTS.DESKTOP_NEXUS_INVALID);	
		} 
		catch (Exception e) {
			logger.error("IO Error while retrieving HTML Document for Wallpaper ID: " + wallpaper.getId());
			listener.metadataDocumentDownloaded(wallpaper, null, EXECUTION_RESULTS.EXCEPTION);
		}
	}
}