package org.wallscrap.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.wallscrap.config.ApplicationConfigReader;
import org.wallscrap.dao.WallpaperDao;
import org.wallscrap.dto.LoginResult;
import org.wallscrap.dto.SearchResult;
import org.wallscrap.dto.WallpaperDTO;
import org.wallscrap.filter.WallpaperFilter;
import org.wallscrap.model.Wallpaper;

@Controller
public class WallpaperController {
	
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired ApplicationConfigReader applicationConfigReader;
	
	@Autowired
	private WallpaperDao wallpaperDao;
	
	@RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public LoginResult login(HttpServletRequest request) {

		logger.debug("Logging in to WallCentral...");
		logger.debug("User IP: " + request.getRemoteAddr());
		
		//create the result object
		LoginResult loginResult = new LoginResult();
		loginResult.setLoginTimestamp(new Timestamp(System.currentTimeMillis()));
		
		//execute the default search
		SearchResult<WallpaperDTO> defaultSearch = search(0, applicationConfigReader.getApplicationConfig().initialSearch);
		loginResult.setDefaultSearchResult(defaultSearch);
		
		return loginResult;
	}
	
	@RequestMapping(value = "/search", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public SearchResult<WallpaperDTO> search(@RequestParam(required=false) Integer offset,@RequestParam(required=false) String keywords) {
		
		logger.debug("Getting wallpapers...  pageNumber: " + offset + ", keyword: " + keywords);
		
		//busqueda por defecto
		if(keywords == null || keywords.trim().equals("")) {
			keywords = applicationConfigReader.getApplicationConfig().initialSearch;
		}
		
		//filtro de busqueda
		WallpaperFilter filter = new WallpaperFilter();
		filter.setFirstResult(offset);
		filter.setMaxResults(applicationConfigReader.getApplicationConfig().searchPageSize);
		filter.setKeywords(keywords);
		
		int total = wallpaperDao.countFindWallpapers(filter);
		List<Wallpaper> wallpapers = new ArrayList<Wallpaper>();
		if(total > 0) {
			wallpapers = wallpaperDao.findWallpapers(filter);	
		}
		
		//creamos resultado a devolver
		SearchResult<WallpaperDTO> result = new SearchResult<WallpaperDTO>();
		result.setTotal(total);
		for(Wallpaper wallpaper : wallpapers) {
			result.getElements().add(new WallpaperDTO(wallpaper));	
		}
		
		return result;
	}
}