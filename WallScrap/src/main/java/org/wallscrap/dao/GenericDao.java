package org.wallscrap.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

public class GenericDao<T> {

	protected final Log logger = LogFactory.getLog(getClass());

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> type;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericDao() {
        Type t = getClass().getGenericSuperclass();
        if (t instanceof ParameterizedType) {
	        ParameterizedType pt = (ParameterizedType) t;
	        type = (Class) pt.getActualTypeArguments()[0];
        } else {
        	type = (Class) t;
        }
    }

	@SuppressWarnings("hiding")
	@Transactional
	public <T> T store(T t) {
		logger.debug("Storing " + type.getSimpleName() + " " + t);
		return entityManager.merge(t);
	}
	
	@Transactional
	public void delete(Long id) {
		logger.debug("Deleting " + type.getSimpleName() + " " + id);
		entityManager.remove(entityManager.find(type, id));
	}

	@Transactional(readOnly = true)
	public T findById(Long id) {
		logger.debug("Find  " + type.getSimpleName() + " " + id);
		return entityManager.find(type, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findAll() {
		logger.debug("Find all " + type.getSimpleName());
		Query query = entityManager.createQuery("from " + type.getSimpleName());
		return query.getResultList();
	}
	
	public void flush() {
		entityManager.flush();
	}
}
