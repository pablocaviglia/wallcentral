package org.wallscrap.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.wallscrap.business.constant.WallpaperScrapStatus;
import org.wallscrap.filter.WallpaperFilter;
import org.wallscrap.model.Wallpaper;

@Repository
public class WallpaperDao extends GenericDao<Wallpaper>{
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Wallpaper> findWallpapers(WallpaperFilter filter) {
		logger.debug("Find wallpapers");
		Query query = buildQueryfindWallpapers(filter, false);
		if (filter.getFirstResult() != null) {
			query.setFirstResult(filter.getFirstResult());
		}
		if (filter.getMaxResults() != null) {
			query.setMaxResults(filter.getMaxResults());
		}

		return query.getResultList();
	}
	
	@Transactional(readOnly = true)
	public int countFindWallpapers(WallpaperFilter filter) {
		logger.debug("Count find wallpapers");
		Query query = buildQueryfindWallpapers(filter, true);
		long result = (Long) query.getSingleResult();
		return (int) result;
	}
	
	@Transactional(readOnly = true)
	public int cambiarEstado(WallpaperScrapStatus estadoActual, WallpaperScrapStatus nuevoEstado) {
		Query query = entityManager.createQuery("update Wallpaper set status = '" + nuevoEstado + "' where status = '" + estadoActual + "'");
		return query.executeUpdate();
	}

	/**
	 * This method build the findWallpaper query.
	 * 
	 * @param filter
	 *            Is a pojo with the fields to filter the Wallpapers
	 * @param count
	 *            Is a boolean to indicate if the query should return the count,
	 *            or the elements
	 * @return Query
	 */
	private Query buildQueryfindWallpapers(WallpaperFilter filter, boolean count) {
		StringBuilder sb = new StringBuilder();
		if (filter.getStatus() != null) {
			sb.append(" where w.status = :status");
		}
		if (filter.getCategory() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.category = :category");
		}
		
		if (filter.getName() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.name = :name ");
		}
		
		if (filter.getKeywords() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" w.keywords like '%" + filter.getKeywords() + "%'");
		}

		sb.insert(0, " from Wallpaper w ");
		
		if (count) {
			sb.insert(0, " select count(w) as qntyWallpapers ");
		}

		Query query = entityManager.createQuery(sb.toString());
		
		if (filter.getStatus() != null) {
			query.setParameter("status", filter.getStatus());
		}
		if (filter.getCategory() != null) {
			query.setParameter("category", filter.getCategory());
		}
		if (filter.getName() != null) {
			query.setParameter("name", filter.getName());
		}
		return query;
	}
}