package org.wallscrap.iface;

import org.jsoup.nodes.Document;
import org.wallscrap.model.Wallpaper;

public interface MetadataDocumentListener {
	
	public enum EXECUTION_RESULTS {OK, EXCEPTION, DESKTOP_NEXUS_INVALID};

	public void metadataDocumentDownloaded(Wallpaper wallpaper, Document wallpaperDocument, EXECUTION_RESULTS executionResult);
	
}
