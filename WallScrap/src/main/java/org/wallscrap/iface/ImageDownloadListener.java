package org.wallscrap.iface;

import org.wallscrap.model.Wallpaper;

public interface ImageDownloadListener {
	
	public enum EXECUTION_RESULTS {OK, BAD, INVALID_ON_DESKTOP_NEXUS};

	public void imageDownloaded(Wallpaper wallpaper, byte[] imageData, EXECUTION_RESULTS executionResult);
	
}