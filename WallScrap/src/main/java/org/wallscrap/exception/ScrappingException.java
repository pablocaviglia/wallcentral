package org.wallscrap.exception;

public class ScrappingException extends Exception {

	private static final long serialVersionUID = -4984738354913765959L;

	public ScrappingException(Exception e) {
		super(e);
	}
	
	public ScrappingException(String msg) {
		super(msg);
	}
}