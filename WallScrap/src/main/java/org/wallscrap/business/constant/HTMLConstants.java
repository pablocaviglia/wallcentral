package org.wallscrap.business.constant;

public interface HTMLConstants {
	static final String HREF = "href";
	static final String TABLE = "table";
	static final String TD = "td";
	static final String ALT = "alt";
}
