package org.wallscrap.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wallscrap.business.constant.WallpaperScrapStatus;
import org.wallscrap.config.XMLScrapConfigReader;
import org.wallscrap.dao.WallpaperDao;
import org.wallscrap.model.Wallpaper;

@Service
@Transactional
public class WallpaperMetadataScraper {
	
	public static Object lock = new Object();

	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private XMLScrapConfigReader xmlScrapConfigReader;

	@Autowired
	private WallpaperDao wallpaperDao;
	
	public Document requestWallpaperMetadataDOM(Wallpaper wallpaper) throws IOException {
		
		String url = wallpaper.getUrl();
		Document metadataPageDocument = null;
		
		try {
			metadataPageDocument = Jsoup.connect(url).get();
		} 
		catch (HttpStatusException e) {
			
			//hubo un error en el parseo del html, debido probablemente 
			//a que el wallpaper ya no existe en el sistema de ellos
			return null;
		}
		
		return metadataPageDocument;
	}
	
	@Transactional(readOnly=false)
	public void scrapWallpaperMetadata(Wallpaper wallpaper, Document metadataPageDocument) {
		
		wallpaper = wallpaperDao.findById(wallpaper.getId());
		
		//data a obtener
		int totalDownloads = 0;
		int timesFavorited = 0;
		String originalResolution = "";
		int width = 0;
		int height = 0;
		long fileSizeBytes = 0;
		String subcategory = "";

		//lista contenedora de tags
		List<String> tagNames = new ArrayList<String>();
		
		//obtenemos los tagslinks
		Elements taglinkElements = metadataPageDocument.getElementsByClass("taglink");
		Iterator<Element> taglinkIt = taglinkElements.iterator();
		while(taglinkIt.hasNext()) {
			Element taglinkElement = taglinkIt.next();
			
			String tagName = taglinkElement.text();
			if(tagName.length() > 256) {
				tagName = tagName.substring(0, 255);
			}
			
			tagNames.add(tagName);
		}
		
		//creamos la url de la SMALL thumbnail (es calculada)
		String thumbUrl = "http://assets.desktopnexus.com/thumbnails/" + wallpaper.getId() + "-thumbnail.jpg";
		
		//buscamos el elemento contenedor de la metadata
		Element metadataElement = null;
		Elements imgElements = metadataPageDocument.getElementsByTag("img");
		Iterator<Element> imgElementsIt = imgElements.iterator();
		while(imgElementsIt.hasNext()) {
			
			Element imgElement = imgElementsIt.next();
			String imgSrc = imgElement.attr("src");
			
			//imagen a la izq de 'total downloads'
			if(imgSrc.trim().equals("http://cache.desktopnexus.com/images/downloads.gif")) {
				metadataElement = imgElement.parent();
				break;
			}
		}
		
		//obtenemos la informacion que define la subcategoria
		imgElementsIt = imgElements.iterator();
		while(imgElementsIt.hasNext()) {
			
			Element imgElement = imgElementsIt.next();
			String imgSrc = imgElement.attr("src");
			
			//imagen a la izq de 'total downloads'
			if(imgSrc.trim().equals("http://cache.desktopnexus.com/images/folder.gif")) {
				Element subcategoryA = imgElement.nextElementSibling();
				subcategory = subcategoryA.text();
				break;
			}
		}
		
		String metadataElementString = metadataElement.toString();
		String[] metadataStringLines = metadataElementString.split("\n");
		for(String metadataLine : metadataStringLines) {
			
			String tmpStr = null;
			
			if(metadataLine.toLowerCase().indexOf("total downloads") != -1) {
				
				tmpStr = metadataLine.substring(metadataLine.toLowerCase().indexOf("total downloads"));
				tmpStr = tmpStr.substring(tmpStr.indexOf(":") + 1).trim().replaceFirst(",", "");
				totalDownloads = Integer.parseInt(tmpStr);
			}
			else if(metadataLine.toLowerCase().indexOf("times favorited") != -1) {
				
				tmpStr = metadataLine.substring(metadataLine.toLowerCase().indexOf("times favorited"));
				tmpStr = tmpStr.substring(tmpStr.indexOf(":") + 1).trim();
				timesFavorited = Integer.parseInt(tmpStr);
			}
			else if(metadataLine.toLowerCase().indexOf("original resolution") != -1) {
				
				tmpStr = metadataLine.substring(metadataLine.toLowerCase().indexOf("original resolution"));
				tmpStr = tmpStr.substring(tmpStr.indexOf(":") + 1).trim();
				originalResolution = tmpStr.trim();
				width = Integer.parseInt(originalResolution.split("x")[0]);
				height = Integer.parseInt(originalResolution.split("x")[1]);
			}
			else if(metadataLine.toLowerCase().indexOf("file size") != -1) {
				
				tmpStr = metadataLine.substring(metadataLine.toLowerCase().indexOf("file size"));
				tmpStr = tmpStr.substring(tmpStr.indexOf(":") + 1).trim().toLowerCase();
				
				if(tmpStr.indexOf("mb") != -1) {
					fileSizeBytes = (long)(Float.parseFloat(tmpStr.substring(0,tmpStr.indexOf("mb"))) * 1024f * 1024f);
				}
				else if(tmpStr.indexOf("kb") != -1) {
					fileSizeBytes = (long)(Float.parseFloat(tmpStr.substring(0,tmpStr.indexOf("kb"))) * 1024f);
				}
			}
		}
		
		wallpaper.setWidth(width);
		wallpaper.setHeight(height);
		wallpaper.setDownloads(totalDownloads);
		wallpaper.setFileSize(fileSizeBytes);
		wallpaper.setTimesFavorited(timesFavorited);
		wallpaper.setSubcategory(subcategory);
		wallpaper.setStatus(WallpaperScrapStatus.METADATA_RETRIEVAL);
		
		wallpaperDao.store(wallpaper);
		
		logger.debug("Wallpaper ID " + wallpaper.getId() + "\tSCRAPED! " + wallpaper.getName() + " ------ " + ((fileSizeBytes/1024) + "kb") + "\t" + totalDownloads + "\t" + originalResolution + "\t" + timesFavorited + "\t" + tagNames);
	}
}