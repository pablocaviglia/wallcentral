package org.wallscrap.business;

import static org.wallscrap.business.constant.HTMLConstants.ALT;
import static org.wallscrap.business.constant.HTMLConstants.HREF;
import static org.wallscrap.business.constant.HTMLConstants.TABLE;
import static org.wallscrap.business.constant.HTMLConstants.TD;

import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wallscrap.business.constant.WallpaperScrapStatus;
import org.wallscrap.config.XMLScrapConfigReader;
import org.wallscrap.dao.WallpaperDao;
import org.wallscrap.exception.ScrappingException;
import org.wallscrap.filter.WallpaperFilter;
import org.wallscrap.model.Category;
import org.wallscrap.model.Wallpaper;
import org.wallscrap.util.ThreadUtils;

@Service
public class WallpaperCategoryScraper {

	private final Log logger = LogFactory.getLog(getClass());
	
	private static final String HTTP_PROTOCOL = "http://" ;
	private static final String DESKTOPNEXUS_DOMAIN = ".desktopnexus.com/";
	private static final String FOLDER_ALL = "all/";
	private static final int FIRST_TD = 0;
	private static final int SLEEP_TIME = 1000;
	private static final String FOLDER_WALLPAPER = "wallpaper/";

	@Autowired
	private XMLScrapConfigReader xmlScrapConfigReader;

	@Autowired
	private WallpaperDao wallpaperDao;

	/**
	 * Scrap all Wallpapers that belong to the categories configurated in the scrapconfig.xml
	 * 
	 * @throws ScrappingException
	 */
	public void scrapWallpaperCategories() throws ScrappingException {
		List<Category> categories = xmlScrapConfigReader.getScrapConfig().getCategories();
		for (Category category : categories) {
			scrapWallpaperCategory(category);
		}
	}
	
	/**
	 * Scrap the wallpapers that belong to the category passed through parameter
	 * 
	 * @param category Is the category we want to scrap
	 * @throws ScrappingException
	 */
	private void scrapWallpaperCategory(Category category) throws ScrappingException {
		int pageSize = xmlScrapConfigReader.getScrapConfig().getElementsPerPage();
		int pages = category.getPages();
		WallpaperFilter filter = new WallpaperFilter();
		filter.setCategory(category.getName());
		int pagesRetrieved = wallpaperDao.countFindWallpapers(filter) / pageSize;

		for (int pageIndex = pagesRetrieved; pageIndex <= pages; pageIndex++) {
			try {
				scrapWallpaperCategoryPage(category, pageIndex);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				pageIndex--;
			}
			ThreadUtils.sleep(SLEEP_TIME);
		}
	}
	
	/**
	 * Scrap the wallpapers that belong to the category passed through parameter
	 * 
	 * @param category Is the category we want to scrap
	 * @throws ScrappingException
	 */
	private void scrapWallpaperCategoryPage(Category category, int pageIndex) throws Exception {
		logger.debug("   scrapping page... " + pageIndex + " (" + category.getName() + ")");
		
		String categoryPage = HTTP_PROTOCOL + category.getSubdomain() + DESKTOPNEXUS_DOMAIN + FOLDER_ALL + pageIndex;
		Document categoryPageDocument = Jsoup.connect(categoryPage).get();
		Elements tableElements = categoryPageDocument.getElementsByTag(TABLE);
		Element wallpapersTableElement = tableElements.get(1);
		Elements wallpapersTDElements = wallpapersTableElement.getElementsByTag(TD);
		int wallsQnty = wallpapersTDElements.size();

		for (int tdWallpaperIndex = 0; tdWallpaperIndex < wallsQnty; tdWallpaperIndex++) {
			Element wallpaperTD = wallpapersTDElements.get(tdWallpaperIndex);

			Element wallpaperA = wallpaperTD.child(FIRST_TD);

			String wallpaperURL = wallpaperA.attr(HREF);
			String wallpaperName = wallpaperA.attr(ALT);
			String wallpaperId = extractWallpaperIDFromURL(wallpaperURL);

			Wallpaper wallpaper = new Wallpaper();
			wallpaper.setDesktopNexusId(Long.valueOf(wallpaperId));
			wallpaper.setFilename(UUID.randomUUID().toString());
			wallpaper.setCategory(category.getName());
			wallpaper.setName(wallpaperName);
			wallpaper.setUrl(wallpaperURL);
			wallpaper.setStatus(WallpaperScrapStatus.FIRST_REGISTRATION);

			wallpaper = wallpaperDao.store(wallpaper);
			
			long id = wallpaper.getId();
			long maxfolder = 1000;
			long folderLong = id / maxfolder;
			wallpaper.setFolder(String.valueOf(folderLong));
			wallpaper = wallpaperDao.store(wallpaper);
			
			logger.debug(wallpaperId + "\t" + wallpaperURL + "\t " + wallpaperName);
		}
	}

	/**
	 * Extract the wallpaperId from the url
	 * @param url The url of the wallpaper, for example http://abstract.desktopnexus.com/wallpaper/305685/
	 * @return Return the wallpaper ID, the number at the end of the url.
	 */
	private String extractWallpaperIDFromURL(String url) {
		String id = "";
		int index = url.indexOf(FOLDER_WALLPAPER) + FOLDER_WALLPAPER.length();
		
		while (Character.isDigit(url.charAt(index))) {
			id += url.charAt(index);
			index++;
		}

		return id;
	}
}