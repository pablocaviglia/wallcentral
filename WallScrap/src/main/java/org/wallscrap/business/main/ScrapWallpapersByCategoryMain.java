package org.wallscrap.business.main;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.wallscrap.business.WallpaperCategoryScraper;

public class ScrapWallpapersByCategoryMain {
	
	private static final String APPLICATION_CONTEXT_XML = "application-context.xml";
	private static final Log logger = LogFactory.getLog(ScrapWallpapersByCategoryMain.class);

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(APPLICATION_CONTEXT_XML);
			WallpaperCategoryScraper wallpaperCategoryScraper = ctx.getBean(WallpaperCategoryScraper.class);
			wallpaperCategoryScraper.scrapWallpaperCategories();
		} 
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}