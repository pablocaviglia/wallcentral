package org.wallscrap.business.main;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.wallscrap.job.SchedulerWallpaperMetadata;

public class ScrapWallpaperMetadataMain {
	
	private static final String APPLICATION_CONTEXT_XML = "application-context.xml";
	private static final Log logger = LogFactory.getLog(ScrapWallpaperMetadataMain.class);

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(APPLICATION_CONTEXT_XML);
			ctx.getBean(SchedulerWallpaperMetadata.class);
		} 
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}