package org.wallscrap.filter;

import org.wallscrap.business.constant.WallpaperScrapStatus;

public class WallpaperFilter {
	
	private WallpaperScrapStatus status;
	private String category;
	private String name;
	private String keywords;

	/**
	 * firstResult Indicates the index of the firstResult
	 */
	private Integer firstResult;

	/**
	 * maxResult Indicates the maximun number of result to retrieve in a query
	 */
	private Integer maxResults;

	public WallpaperScrapStatus getStatus() {
		return status;
	}

	public void setStatus(WallpaperScrapStatus status) {
		this.status = status;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
}