package org.wallscrap.config;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.wallscrap.model.Category;
import org.wallscrap.model.ScrapConfig;

@Repository
public class XMLScrapConfigReader {

	private final Log logger = LogFactory.getLog(getClass());

	private final String FILE_PATH_SCRAP_CONFIG = "classpath:/scrapconfig.xml";
	private final Resource scrapConfigXML = (new DefaultResourceLoader()).getResource(FILE_PATH_SCRAP_CONFIG);
	private ScrapConfig scrapConfig;
	
	public XMLScrapConfigReader() {
		leerScrapConfig();
	}
	
	public ScrapConfig getScrapConfig() {
		return scrapConfig;
	}
	
	private void leerScrapConfig() {
		
		logger.info("XMLScrapConfigImpl.obtenerScrapConfig");
		try {
			
			//lectura de categorias
			List<Category> categories = new ArrayList<Category>();

			SAXBuilder builder = new SAXBuilder(false);
			Document document = builder.build(scrapConfigXML.getFile());
			
			Element root = document.getRootElement();
			
			//obtenemos el atributo que define
			//la cantidad de elementos por pagina
			Integer elementsPerPage = Integer.parseInt(root.getAttributeValue("elementsPerPage"));
			String thumbnailFolder = root.getAttributeValue("thumbnailFolder");
			
			//obtenemos la lista de categorias
			Element categoriesElement = root.getChild("categories");
			@SuppressWarnings("unchecked")
			List<Element> categoryElementList = categoriesElement.getChildren("category");
			Iterator<Element> it = categoryElementList.iterator();
			while (it.hasNext()) {
				Element categoryElement = it.next();
				categories.add(crearCategory(categoryElement));
			}
			
			scrapConfig = new ScrapConfig();
			scrapConfig.setElementsPerPage(elementsPerPage);
			scrapConfig.setThumbnailFolder(thumbnailFolder);
			scrapConfig.setCategories(categories);
			
		} 
		catch (Exception e) {
			logger.error("File not found in path: " + FILE_PATH_SCRAP_CONFIG, e);
		}
	}

	private Category crearCategory(Element categoryElement) {
		
		String name = categoryElement.getAttributeValue("name").trim();
		String subdomain = categoryElement.getAttributeValue("subdomain").trim();
		int pages = Integer.parseInt(categoryElement.getAttributeValue("pages").trim());
		
		Category category = new Category();
		category.setName(name);
		category.setSubdomain(subdomain);
		category.setPages(pages);
		
		return category;
	}
}