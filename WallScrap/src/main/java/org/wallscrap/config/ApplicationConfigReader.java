package org.wallscrap.config;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.wallscrap.model.ApplicationConfig;

@Repository
public class ApplicationConfigReader {

	private static final Log logger = LogFactory.getLog(ApplicationConfigReader.class);
	private static final String FILE_PATH_APPLICATION_CONFIG = "classpath:/applicationconfig.xml";
	private static final Resource applicationConfigXML = (new DefaultResourceLoader()).getResource(FILE_PATH_APPLICATION_CONFIG);
	private static ApplicationConfig applicationConfig;
	
	public ApplicationConfigReader() {
		if(applicationConfig == null) {
			//lee el archivo de configuracion
			leerApplicationConfig();
		}
	}
	
	public ApplicationConfig getApplicationConfig() {
		return applicationConfig;
	}
	
	private static void leerApplicationConfig() {
		
		logger.info("ApplicationConfigReader.leerApplicationConfig");
		try {

			applicationConfig = new ApplicationConfig();
			
			SAXBuilder builder = new SAXBuilder(false);
			Document document = builder.build(applicationConfigXML.getFile());
			Element root = document.getRootElement();
			
			//obtenemos la lista de categorias
			@SuppressWarnings("unchecked")
			List<Element> configElements = root.getChildren("config");
			for(Element configElement : configElements) {
				
				String id = configElement.getAttributeValue("id");
				String value = configElement.getAttributeValue("value");
				
				if(id.trim().equalsIgnoreCase("INITIAL_SEARCH")) {
					applicationConfig.initialSearch = value;
				}
				else if(id.trim().equalsIgnoreCase("SHARE_WALLPAPER_URL")) {
					applicationConfig.shareWallpaperURL = value;
				}
				else if(id.trim().equalsIgnoreCase("SEARCH_PAGE_SIZE")) {
					applicationConfig.searchPageSize = Integer.parseInt(value);
				}
			}
		} 
		catch (Exception e) {
			logger.error("File not found in path: " + FILE_PATH_APPLICATION_CONFIG, e);
		}
	}
}