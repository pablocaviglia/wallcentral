package org.wallscrap.dto;

import java.io.Serializable;

import org.wallscrap.model.Wallpaper;

public class WallpaperDTO implements Serializable {

	private static final long serialVersionUID = 2476106909138840465L;
	
	private long id;
	private String name;
	private String path;
	private String key;
	
	public WallpaperDTO(long id, String name, String key, String path) {
		this.id = id;
		this.name = name;
		this.key = key;
		this.path = path;
	}
	
	public WallpaperDTO(Wallpaper wallpaper) {
		this.id = wallpaper.getId();
		this.name = wallpaper.getName();
		this.key = wallpaper.getFilename();
		this.path = wallpaper.getFolder();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}