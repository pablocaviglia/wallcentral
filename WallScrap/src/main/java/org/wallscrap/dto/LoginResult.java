package org.wallscrap.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class LoginResult implements Serializable {

	private static final long serialVersionUID = 7155178130049181834L;
	
	private Timestamp loginTimestamp;
	private SearchResult<WallpaperDTO> defaultSearchResult;
	
	public Timestamp getLoginTimestamp() {
		return loginTimestamp;
	}
	public void setLoginTimestamp(Timestamp loginTimestamp) {
		this.loginTimestamp = loginTimestamp;
	}
	public SearchResult<WallpaperDTO> getDefaultSearchResult() {
		return defaultSearchResult;
	}
	public void setDefaultSearchResult(
			SearchResult<WallpaperDTO> defaultSearchResult) {
		this.defaultSearchResult = defaultSearchResult;
	}
}