package com.gmi.wallcentralcliententertainment;
import com.gmi.wallcentralclientbase.App;

public class EntertainmentApp extends App {
	
	@Override
	public void initialize() {
		App.CATEGORIA_BASE = "entertainment";
	}
}