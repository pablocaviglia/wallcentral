package com.gmi.wallcentralclientarchitecture;
import com.gmi.wallcentralclientbase.App;

public class ArchitectureApp extends App {
	
	@Override
	public void initialize() {
		App.CATEGORIA_BASE = "architecture";
	}
}