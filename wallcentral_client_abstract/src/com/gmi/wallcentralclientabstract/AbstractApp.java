package com.gmi.wallcentralclientabstract;
import com.gmi.wallcentralclientbase.App;

public class AbstractApp extends App {
	
	@Override
	public void initialize() {
		App.CATEGORIA_BASE = "abstract";
		App.ADMOB_BANNER_ID = "";//"ca-app-pub-6328885496949159/6268521826";
		App.ADMOB_INTERSTITIAL_UNIT_ID = "";//"ca-app-pub-6328885496949159/7745255024";
	}
}