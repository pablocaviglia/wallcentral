package com.gmi.wallcentralclientbase.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gmi.wallcentralclientbase.domain.Wallpaper;

public class PersistenceHelper {

	private static final int DATABASE_VERSION = 200;
	public SQLiteDatabase db = null;
	
	//table names
	private static final String TABLE_CONFIG = "CONFIG";
	private static final String TABLE_WALLPAPER = "WALLPAPER";
	
	//ids de valores en tabla config
	public static int CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD = 1;
	public static int CONFIG_CALIFICA_APLICACION = 2;
	
    //statements for creating tables
    private static final String CREATE_TABLE_CONFIG = 
    			"CREATE TABLE " + 
    			TABLE_CONFIG + " ( " +
            	"ID INTEGER PRIMARY KEY NOT NULL, " + 
            	"VALUE VARCHAR(512));";

    private static final String CREATE_TABLE_WALLPAPER = 
    			"CREATE TABLE " + 
    			TABLE_WALLPAPER + " ( " +
            	"ID 	INTEGER PRIMARY KEY NOT NULL, " + 
            	"NAME 	VARCHAR(512), " +
            	"PATH 	VARCHAR(32), " +
            	"KEY 	VARCHAR(128));";

    public PersistenceHelper(Context context) {
		SQLiteOpenHelper o = new PersistenceOpenHelper(context, "wallcentral.db");
		this.db = o.getWritableDatabase();
	}

	// Rest of standard DataHelper class
	private class PersistenceOpenHelper extends SQLiteOpenHelper {

		PersistenceOpenHelper(Context context, String databaseName) {
			super(context, databaseName, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			
			//drop and create tableS
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIG);
	        db.execSQL(CREATE_TABLE_CONFIG);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_WALLPAPER);
	        db.execSQL(CREATE_TABLE_WALLPAPER);
	        
	        //insert default 'CONFIG' values
	        db.execSQL("INSERT INTO CONFIG(ID, VALUE) VALUES(" + CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD + ", '0');");
	        db.execSQL("INSERT INTO CONFIG(ID, VALUE) VALUES(" + CONFIG_CALIFICA_APLICACION + ", '30');");
	        
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			onCreate(db);
		}
	}
	
	public String obtenerValorConfig(int key) {
		
		String value = null;
	    Cursor cursor = db.query(TABLE_CONFIG, 
				 new String[] {"VALUE"}, 
				 "ID=?",
				 new String[] { String.valueOf(key) }, null, null, null, null);

	    if (cursor != null && cursor.moveToFirst()) {
	    	value = cursor.getString(0);
	    }
	    
		return value;
	}
	
	public void actualizarValorConfig(int key, String value) {
		ContentValues args = new ContentValues();
	    args.put("VALUE", value);
	    db.update(TABLE_CONFIG, args, "ID="+key, null);
	}
	
	public Integer obtenerCantidadWallpapersDescargadosSinMostrarRectangle() {
		return Integer.valueOf(obtenerValorConfig(CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD));
	}
	
	public Integer obtenerCalificaAplicacion() {
		return Integer.valueOf(obtenerValorConfig(CONFIG_CALIFICA_APLICACION));
	}

	public void decrementarConfigCalificaAplicacion() {
		
		//decrementamos
		Integer tmpInt = Integer.parseInt(obtenerValorConfig(CONFIG_CALIFICA_APLICACION)) - 1;
		
		//corregimos para no
		//bajar menos de cero
		if(tmpInt < 0) {
			tmpInt = 0;
		}
		
		//actualizamos valor
		actualizarValorConfig(CONFIG_CALIFICA_APLICACION, tmpInt.toString());
	}
	
	public void resetConfigCalificaAplicacion() {
		actualizarValorConfig(CONFIG_CALIFICA_APLICACION, "0");
	}
	
	public void resetConfigWallpapersDescargadosSinCliquearAd() {
		actualizarValorConfig(CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD, "0");
	}
	
	public void incrementarConfigWallpapersDescargadosSinCliquearAd() {
		String tmp = obtenerValorConfig(CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD);
		Integer cant = Integer.parseInt(tmp) + 1;
		actualizarValorConfig(CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD, cant.toString());
	}
	
	public void guardarWallpaper(Wallpaper wallpaper) {
		
		try {
			ContentValues wallpaperValues = new ContentValues(); 
			wallpaperValues.put("ID", wallpaper.id);
			wallpaperValues.put("NAME", wallpaper.name);
			wallpaperValues.put("PATH", wallpaper.path);
			wallpaperValues.put("KEY", wallpaper.key);
			
			db.insert(TABLE_WALLPAPER, null, wallpaperValues);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Wallpaper obtenerWallpaper(int id) {
		
		Wallpaper wallpaper = null;
		try {
		    Cursor cursor = db.rawQuery("SELECT NAME, PATH, KEY FROM " + TABLE_WALLPAPER + " WHERE ID = " + id, null);
		    if (cursor != null && cursor.moveToFirst()) {
		    	String name = cursor.getString(0);
		    	String path = cursor.getString(1);
		    	String key = cursor.getString(2);
		    	wallpaper = new Wallpaper(id, name, path, key);
		    }
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return wallpaper;
	}
}