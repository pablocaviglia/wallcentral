package com.gmi.wallcentralclientbase.domain;

import java.io.Serializable;

public class ResultMetadata implements Serializable {

	private static final long serialVersionUID = -2541953834106407739L;
	
	public int id;
	public String name;
	public String path;
	public String key;
	public Integer downloads;
	public String keywords;
	public String category;
	
}
