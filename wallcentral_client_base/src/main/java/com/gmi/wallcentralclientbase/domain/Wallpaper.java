package com.gmi.wallcentralclientbase.domain;

import java.io.File;
import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.gmi.wallcentralclientbase.App;

public class Wallpaper implements Parcelable, Serializable {

	private static final long serialVersionUID = 8655679371371721930L;
	
	public int id;
	public String name;
	public String path;
	public String key;
	public String archivoDirecto;
	
	//ultima fecha de modificacion
	//del archivo en el FS
	public Long fileLastMod;
	
	public int downloadThumbRetry; 
	
	public Wallpaper(int id, String name, String path, String key) {
		this.id = id;
		this.name = name;
		this.path = path;
		this.key = key;
	}
	
	public Wallpaper(Parcel parcel) {
		this.id = parcel.readInt();
		this.name = parcel.readString();
		this.path = parcel.readString();
		this.key = parcel.readString();
		this.archivoDirecto = parcel.readString();
	}
	
	public String obtenerUrlThumb() {
		return App.SERVER_BASE + "/wallsThumb/" + path + "/" + key; 
	}
	
	public String obtenerUrl(int screenWidth, int screenHeight) {
		String url = App.SERVER_APPLICATION_CONTEXT + 
					 "/action/image/get/" + id + "/" + 
					 (screenWidth * 2) + "/" + 
					 screenHeight;
		
		return url;
	}
	
	public String obtenerNombreArchivo() {
		return id + App.FILENAME_EXTENSION;
	}
	
	public File obtenerArchivo() {
		if(archivoDirecto != null && new File(archivoDirecto).exists()) {
			return new File(archivoDirecto);
		}
		else {
			return new File(App.WALLPAPERS_OUTPUT_FOLDER, "/" + obtenerNombreArchivo());	
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(path);
		dest.writeString(key);
		dest.writeString(archivoDirecto);
	}
	
    public static final Parcelable.Creator<Wallpaper> CREATOR = new Parcelable.Creator<Wallpaper>() {
        public Wallpaper createFromParcel(Parcel in) {
            return new Wallpaper(in);
        }

        public Wallpaper[] newArray(int size) {
            return new Wallpaper[size];
        }
    };
}