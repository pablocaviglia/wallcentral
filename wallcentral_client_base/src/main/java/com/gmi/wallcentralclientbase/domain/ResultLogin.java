package com.gmi.wallcentralclientbase.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class ResultLogin implements Serializable {
	
	private static final long serialVersionUID = -1520870898305081871L;
	
	public Timestamp loginTimestamp;
	public String defaultSearch;
	public ResultObtenerWallpapers defaultSearchResult;
	public boolean loginStatusOk;
	public List<String> categorias;
	
	public boolean error;
	
}