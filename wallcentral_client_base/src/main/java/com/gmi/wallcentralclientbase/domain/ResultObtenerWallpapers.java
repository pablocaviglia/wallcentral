package com.gmi.wallcentralclientbase.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class ResultObtenerWallpapers implements Serializable {

	private static final long serialVersionUID = -6301262111910738281L;

	public boolean error;
	public int total;
	public ArrayList<Wallpaper> wallpapers;
	
}
