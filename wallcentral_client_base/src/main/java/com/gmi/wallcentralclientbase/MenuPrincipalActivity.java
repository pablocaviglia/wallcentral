package com.gmi.wallcentralclientbase;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.gmi.wallcentralclientbase.db.PersistenceHelper;
import com.gmi.wallcentralclientbase.domain.ResultLogin;
import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.listener.LogicFacadeListener;
import com.gmi.wallcentralclientbase.ui.adapter.MenuPrincipalPagerAdapter;
import com.gmi.wallcentralclientbase.ui.adapter.PreviewViewPagerAdapter;
import com.gmi.wallcentralclientbase.ui.view.ViewAnticipo;
import com.gmi.wallcentralclientbase.ui.view.ViewBuscar;
import com.gmi.wallcentralclientbase.ui.view.ViewCategorias;
import com.gmi.wallcentralclientbase.ui.view.ViewMetadata;
import com.gmi.wallcentralclientbase.ui.view.ViewMisDescargas;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MenuPrincipalActivity extends SherlockFragmentActivity implements SearchView.OnQueryTextListener, LogicFacadeListener {
	
	public static byte CANTIDAD_COLUMNAS_SMALL_MEDIUM = 2;
	public static byte CANTIDAD_COLUMNAS_BIG = 3;
	public static final boolean PAUSE_ONSCROLL = false;
	public static final boolean PAUSE_ONFLING = true;
	
	//admob ads references
	private InterstitialAd interstitial;
	
	//pager adapters
	private MenuPrincipalPagerAdapter menuPrincipalPagerAdapter;
	private PreviewViewPagerAdapter previewPagerAdapter;
	
	//resultado del login
	private ResultLogin resultLogin;
	
	//busqueda ejecutada desde
	//la pantalla de preview
	public static String busquedaPreview;
	
	//popup que muestra 
	//informacion de los ads
	public PopupWindow popupWindow;
	
	//contexto actual de la aplicacion
	private static final byte CONTEXTO_MENU_PRINCIPAL = 0;
	private static final byte CONTEXTO_PREVIEW = 1;
	private byte contextoActual; 
	
	//tab actual del menu principal
	private static final byte TAB_MENU_PRINCIPAL_POPULAR = 0;
	private static final byte TAB_MENU_PRINCIPAL_BUSCAR = 1;
	private static final byte TAB_MENU_PRINCIPAL_MIS_DESCARGAS = 2;
	private static final byte TAB_MENU_PRINCIPAL_CATEGORIAS = 3;
	public byte tabMenuActual; 
	
	private NetworkChangeReceiver receiver;
	private boolean isConnected = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		//seteamos la vista de contenido
		setContentView(R.layout.activity_main);
		
		//creamos los adapter de vistas
		menuPrincipalPagerAdapter = new MenuPrincipalPagerAdapter(this, savedInstanceState);
		previewPagerAdapter = new PreviewViewPagerAdapter(this, savedInstanceState);
		
        //activate Navigation Mode Tabs
		ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
		//obtenemos el paginador 
        ViewPager pager = (ViewPager)findViewById(R.id.mainmenu_pager);
		
        //indicamos el listener de cambio de pagina
        pager.setOnPageChangeListener(new SimpleOnPageChangeListener(actionBar));
        
        //mostramos la vista del menu principal
		mostrarVistaMenuPrincipal(TAB_MENU_PRINCIPAL_BUSCAR);
		
        //cargamos los wallpapers por defecto
		Bundle extras = getIntent().getExtras();
		if(savedInstanceState == null && extras != null) {
			//cargamos el login result
			resultLogin = (ResultLogin) extras.getSerializable(App.BUNDLE_DEFAULT_SEARCH_RESULT);
			cargarResultLogin(resultLogin);
		}
	
		IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		receiver = new NetworkChangeReceiver();
		registerReceiver(receiver, filter);
		
        //create the admob banner
		createAdmodBanner();
		
		//load the ads
		loadBanner();
		loadInterstitial();
		
    }
	
	private void guardarTabActual() {
		if(contextoActual == CONTEXTO_MENU_PRINCIPAL) {
			ActionBar actionBar = getSupportActionBar();
			switch(actionBar.getSelectedTab().getPosition()) {
			case TAB_MENU_PRINCIPAL_POPULAR:
				tabMenuActual = TAB_MENU_PRINCIPAL_POPULAR;
				break;
			case TAB_MENU_PRINCIPAL_BUSCAR:
				tabMenuActual = TAB_MENU_PRINCIPAL_BUSCAR;
				break;
			case TAB_MENU_PRINCIPAL_MIS_DESCARGAS:
				tabMenuActual = TAB_MENU_PRINCIPAL_MIS_DESCARGAS;
				break;
			case TAB_MENU_PRINCIPAL_CATEGORIAS:
				tabMenuActual = TAB_MENU_PRINCIPAL_CATEGORIAS;
				break;
			}
		}
	}
	
	public void mostrarVistaMenuPrincipal(byte tabSeleccionado) {
		
		contextoActual = CONTEXTO_MENU_PRINCIPAL;
		
        //activate Navigation Mode Tabs
		ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        //sacamos los tab anteriores
        actionBar.removeAllTabs();
        
        //obtenemos el paginador 
        ViewPager pager = (ViewPager)findViewById(R.id.mainmenu_pager);
        
        //capture tab button clicks
        TabListener tabListener = new TabListener(pager);
        
        //crea el tab popular
        Tab popularTab = actionBar.newTab().setText(getResources().getString(R.string.popular)).setTabListener(tabListener);
        actionBar.addTab(popularTab);
        
        //crea el tab de busqueda
        Tab buscarTab = actionBar.newTab().setText(getResources().getString(R.string.search)).setTabListener(tabListener);
        actionBar.addTab(buscarTab);
        
        //crea el tab de mis descargas
        Tab misDescargasTab = actionBar.newTab().setText(getResources().getString(R.string.my_downloads)).setTabListener(tabListener);
        misDescargasTab.setTag("mis_descargas");
        actionBar.addTab(misDescargasTab);
        
        //crea el tab categorias
        Tab categoriasTab = actionBar.newTab().setText(getResources().getString(R.string.categories)).setTabListener(tabListener);
        actionBar.addTab(categoriasTab);
        
        // Set the View Pager Adapter into ViewPager
        pager.setOffscreenPageLimit(menuPrincipalPagerAdapter.getCount());  
        pager.setAdapter(menuPrincipalPagerAdapter);
        
        //seteamos el tab elegido
		switch(tabSeleccionado) {
		case TAB_MENU_PRINCIPAL_POPULAR:
	        actionBar.selectTab(popularTab);
	        break;
		case TAB_MENU_PRINCIPAL_BUSCAR:
	        actionBar.selectTab(buscarTab);
	        break;
		case TAB_MENU_PRINCIPAL_MIS_DESCARGAS:
	        actionBar.selectTab(misDescargasTab);
	        break;
		case TAB_MENU_PRINCIPAL_CATEGORIAS:
	        actionBar.selectTab(categoriasTab);
	        break;
		}
	}
	
	public void mostrarVistaPreview(Wallpaper wallpaper) {
		
		//guardamos el tab elegido en el
		//contexto anterior
		guardarTabActual();
		
		contextoActual = CONTEXTO_PREVIEW;
		
        //activate Navigation Mode Tabs
		ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        //sacamos los tab anteriores
        actionBar.removeAllTabs();
        
        //obtenemos el paginador 
        ViewPager pager = (ViewPager)findViewById(R.id.mainmenu_pager);
        
        //capture tab button clicks
        TabListener tabListener = new TabListener(pager);
        
        //crea el tab anticipo
        Tab anticipoTab = actionBar.newTab().setText(getResources().getString(R.string.preview)).setTabListener(tabListener);
        actionBar.addTab(anticipoTab);
        
        //crea el tab de busqueda
        Tab metadataTab = actionBar.newTab().setText(getResources().getString(R.string.metadata)).setTabListener(tabListener);
        actionBar.addTab(metadataTab);
        
        // Set the View Pager Adapter into ViewPager
        pager.setOffscreenPageLimit(previewPagerAdapter.getCount());  
        pager.setAdapter(previewPagerAdapter);
        
        //mostramos el wallpaper
        ViewAnticipo viewAnticipo = (ViewAnticipo) previewPagerAdapter.obtenerVista(PreviewViewPagerAdapter.VIEW_ANTICIPO);
        viewAnticipo.iniciarPreviewImagen(wallpaper);
        
        ViewMetadata viewMetadata = (ViewMetadata) previewPagerAdapter.obtenerVista(PreviewViewPagerAdapter.VIEW_METADATA);
        viewMetadata.obtenerMetadata(wallpaper);

        //elije el primer tab
        actionBar.selectTab(anticipoTab);
        
	}
    
	public void createAdmodBanner() {
		if(App.ADMOB_BANNER_ID != null && !App.ADMOB_BANNER_ID.trim().equals("")) {
		   	final AdView admobBanner = new AdView(this);
	    	admobBanner.setAdUnitId(App.ADMOB_BANNER_ID);
	    	admobBanner.setAdSize(AdSize.SMART_BANNER);
	    	
	    	admobBanner.setAdListener(new AdListener() {
	    		@Override
	    		public void onAdFailedToLoad(int errorCode) {
	    			super.onAdFailedToLoad(errorCode);
	    			//retry load
	    			admobBanner.postDelayed(new Runnable() {
						@Override
						public void run() {
							System.out.println("------ RECARGANDO AD DEBIDO A ERROR EN CARGA");
							loadBanner();
						}
					}, 5000);
	    		}
	    		@Override
	    		public void onAdLeftApplication() {
	    			super.onAdLeftApplication();
	    			App.data.resetConfigWallpapersDescargadosSinCliquearAd();
	    			if(contextoActual == CONTEXTO_PREVIEW) {
	    				popupWindow.dismiss();
	    				ViewAnticipo viewAnticipo = (ViewAnticipo) previewPagerAdapter.obtenerVista(PreviewViewPagerAdapter.VIEW_ANTICIPO);
	    				viewAnticipo.descargarWallpaper();
	    				detenerRelampaguearAdmobView();
	    			}
	    		}
			});
	    	
			//get the adview layout
	    	LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.adMainContainer);
	    	
	    	//remove possible old views
	    	comicMenuWebLayout.removeAllViews();
	    	
			//show the adview on the UI
			//only if its loading process
			//is correctly finished
			comicMenuWebLayout.addView(admobBanner);
		}
	}
	
	public void loadBanner() {
		if(App.ADMOB_BANNER_ID != null && !App.ADMOB_BANNER_ID.trim().equals("")) {
			//get the adview layout
	    	LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.adMainContainer);
	    	AdView bannerAdView = (AdView)comicMenuWebLayout.getChildAt(0);
	    	bannerAdView.loadAd(new AdRequest.Builder().build());
		}
	}
	
	public void loadInterstitial() {
		if(App.ADMOB_INTERSTITIAL_UNIT_ID != null && !App.ADMOB_INTERSTITIAL_UNIT_ID.trim().equals("")) {
	        //create and load the interstitial
	        interstitial = new InterstitialAd(this);
	        interstitial.setAdUnitId(App.ADMOB_INTERSTITIAL_UNIT_ID);
	        interstitial.setAdListener(new AdListener() {
	        	  public void onAdLoaded() {
	        	  }
	        	  public void onAdFailedToLoad(int errorCode) {
	        	  }
	        	  public void onAdOpened() {
	        	  }
	        	  public void onAdClosed() {
        			  exitApplication();
	        	  }
	        	  public void onAdLeftApplication() {
	        	  }
			});
	        
	        //begin loading the interstitial
	        interstitial.loadAd(new AdRequest.Builder().build());
        }
	}
	
	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
	
	private void cargarResultLogin(ResultLogin resultLogin) {
    	
		//si tenemos resultados por defecto
		//cargados durante el login
		if(resultLogin != null && resultLogin.defaultSearchResult != null) {
			
			//seteamos flag que el resultado
			//del login fue satisfactorio
			resultLogin.loginStatusOk = true;
			
			//vamos al menu principal
			mostrarVistaMenuPrincipal(TAB_MENU_PRINCIPAL_BUSCAR);
			MenuPrincipalPagerAdapter pagerAdapter = (MenuPrincipalPagerAdapter)obtenerPagerAdapter();
			
			//cargamos busqueda
			ViewBuscar viewBuscar = (ViewBuscar) pagerAdapter.obtenerVista(MenuPrincipalPagerAdapter.VIEW_BUSCAR);
			
			//cargamos los wallpapers por defecto
			viewBuscar.cargarWallpapers(resultLogin.defaultSearchResult.total, resultLogin.defaultSearch, null, true, resultLogin.defaultSearchResult.wallpapers);
	        
	        //cargamos categorias
	        ViewCategorias viewCategorias = (ViewCategorias) pagerAdapter.obtenerVista(MenuPrincipalPagerAdapter.VIEW_CATEGORIAS);
	        viewCategorias.cargarCategorias(resultLogin.categorias);

		}
    }
    
	public void evaluarMostrarPopupRating() {
    	
        int wallpapersDescargadosSinCalificacion = App.data.obtenerCalificaAplicacion();
        if(wallpapersDescargadosSinCalificacion == 0) {
        	
        	final CharSequence colors[] = new CharSequence[] {
        			getResources().getString(R.string.rateoption_iwant), 
        			getResources().getString(R.string.rateoption_later), 
        			getResources().getString(R.string.rateoption_no)};

        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setTitle(getResources().getString(R.string.rate_application));
        	builder.setItems(colors, new DialogInterface.OnClickListener() {
        	    @Override
        	    public void onClick(DialogInterface dialog, int which) {
        	    	if(which == 0) {
        	        	//no jodemos mas al usuario
        	    		//con este cartel por un buen
        	    		//tiempo
        	        	App.data.actualizarValorConfig(PersistenceHelper.CONFIG_CALIFICA_APLICACION, "1000");
        	        	
        	        	//redirigimos al google play 
        	        	//asi nos votan
        	        	Intent i = new Intent(android.content.Intent.ACTION_VIEW);
        	        	i.setData(Uri.parse(App.GOOGLE_PLAY_APPLICATION_URL));
        	        	startActivity(i);
        	    	}
        	        else if(which == 1) {
        	        	//incrementamos un poco la
        	        	//variable para joderlo mas
        	        	//tarde con este cartel
        	        	App.data.actualizarValorConfig(PersistenceHelper.CONFIG_CALIFICA_APLICACION, "50");
        	        }
        	        else if(which == 2) {
        	        	App.data.actualizarValorConfig(PersistenceHelper.CONFIG_CALIFICA_APLICACION, "200");
        	        }
        	    }
        	});
        	builder.show();
        	
        	//mostramos el toast
        	Toast.makeText(getApplicationContext(), getResources().getString(R.string.rate_helpmsg), Toast.LENGTH_LONG).show();
        	
        }
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	
    	//Create the search view
    	SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
        searchView.setQueryHint(getResources().getString(R.string.search_for));
        searchView.setOnQueryTextListener(this);
        ShareActionProvider shareActionProvider = new ShareActionProvider(getSupportActionBar().getThemedContext());
        shareActionProvider.setShareIntent(getShareAppIntent());
        
        menu.add(getResources().getString(R.string.search))
            .setIcon(R.drawable.abs__ic_search)
            .setActionView(searchView)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        
        menu.add(getResources().getString(R.string.share))
        	.setIcon(R.drawable.abs__ic_menu_share_holo_dark)
        	.setActionProvider(shareActionProvider)
        	.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        
        return true;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		if(contextoActual == CONTEXTO_MENU_PRINCIPAL) {
			((MenuPrincipalPagerAdapter)obtenerPagerAdapter()).onSaveInstanceState(outState);	
		}
		else if(contextoActual == CONTEXTO_PREVIEW) {
			((PreviewViewPagerAdapter)obtenerPagerAdapter()).onSaveInstanceState(outState);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//si venimos desde preview con
	    //una busqueda, la ejecutamos
	    if(busquedaPreview != null) {
	    	iniciarBusqueda(busquedaPreview, null, false);
	    	busquedaPreview = null;
	    }
	}
	
	private class TabListener implements ActionBar.TabListener {
		private ViewPager viewPager;
		public TabListener(ViewPager viewPager) {
			this.viewPager = viewPager;
		}
		@Override
        public void onTabSelected(Tab tab, FragmentTransaction ft) {
			viewPager.setCurrentItem(tab.getPosition());
        	if(tab.getTag() != null && tab.getTag().equals("mis_descargas")) {
            	ViewMisDescargas viewMisDescargas = (ViewMisDescargas) menuPrincipalPagerAdapter.obtenerVista(MenuPrincipalPagerAdapter.VIEW_MISDESCARGAS);
            	viewMisDescargas.notificarCambioContenido();
        	}
        }
        @Override
        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        }
        @Override
        public void onTabReselected(Tab tab, FragmentTransaction ft) {
        }
	}
	
	private class SimpleOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {
		private ActionBar actionBar;
		public SimpleOnPageChangeListener(ActionBar actionBar) {
			this.actionBar = actionBar;
		}
		@Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            actionBar.setSelectedNavigationItem(position);
        }
	}

    @Override
	public boolean onQueryTextSubmit(String query) {
		iniciarBusqueda(query, null, false);
		return true;
	}
    
    public void iniciarBusqueda(String keywords, String categoria, boolean popular) {
    	
    	//seteamos titulo de busqueda
    	if(keywords != null || categoria != null) {
        	setSearchTitle(keywords != null ? keywords : categoria);
    	}
    	
    	//nos movemos al menu principal
    	mostrarVistaMenuPrincipal(TAB_MENU_PRINCIPAL_BUSCAR);
    	MenuPrincipalPagerAdapter adapter = (MenuPrincipalPagerAdapter) obtenerPagerAdapter();
    	
		//ejecutamos la busqueda en
		//el fragmento de busqueda
    	ViewPager pager = (ViewPager)findViewById(R.id.mainmenu_pager);
        pager.setCurrentItem(MenuPrincipalPagerAdapter.VIEW_BUSCAR);
        ViewBuscar viewBuscar = (ViewBuscar) adapter.obtenerVista(MenuPrincipalPagerAdapter.VIEW_BUSCAR);
        viewBuscar.iniciarBusqueda(keywords, categoria, popular);
        
    }
    
    private PagerAdapter obtenerPagerAdapter() {
    	PagerAdapter pagerAdapter = null;
    	ViewPager pager = (ViewPager)findViewById(R.id.mainmenu_pager);
    	if(contextoActual == CONTEXTO_MENU_PRINCIPAL) {
            pagerAdapter = (MenuPrincipalPagerAdapter) pager.getAdapter();
    	}
    	else {
    		pagerAdapter = (PreviewViewPagerAdapter) pager.getAdapter();
    	}
        return pagerAdapter;
    }
    
    public void setSearchTitle(String value) {
	    getSupportActionBar().setTitle(value);
    }
    
	@Override
	public boolean onQueryTextChange(String newText) {
		return true;
	}
	
    private Intent getShareAppIntent() {
    	
    	Intent intent = new Intent();
    	intent.setAction(Intent.ACTION_SEND);
    	intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
    	intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_text) + " " + App.GOOGLE_PLAY_APPLICATION_URL);
    	intent.setType("text/plain");
    	
    	return intent;
    }
    
    //flag que indica si debemos de
	//salir de la aplicacion cuando
	//se presione el boton de back
	private boolean exitOnBack;
	
	@Override
	public void onBackPressed() {
		if(contextoActual == CONTEXTO_PREVIEW) {
			if(popupWindow != null && popupWindow.isShowing()) {
				popupWindow.dismiss();
			}
			else {
				mostrarVistaMenuPrincipal(tabMenuActual);	
			}
		}
		else {
			//creamos un handler para que espere
			//un tiempo antes de cambiar el flag
			//que indica si la proxima vez que se
			//presione el boton back se sale de la
			//aplicacion
		    Handler backButtonTimerHandler = new Handler();
			backButtonTimerHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					exitOnBack = false;
				}
			}, 4000);
			
			if(!exitOnBack) {
				//mandamos mensaje 
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.back_to_exit), Toast.LENGTH_SHORT).show();
				//cambiamos flag 
				exitOnBack = true;
				//dont exit
				return;
			}
			else {
				if(interstitial != null && interstitial.isLoaded()) {
					displayInterstitial();
				}
				else {
					exitApplication();	
				}
				
				//cargamos un nuevo interstitial
				loadInterstitial();
			}
		}
	}
	
	private void exitApplication() {
		moveTaskToBack(true);
	}
	
	@Override
	public void operationFinished(Object result, byte operationType) {
    	if(operationType == LogicFacadeListener.OPERATION_Login) {
	        runOnUiThread(new CargarResultLoginTask((ResultLogin)result));
		}
	}
    
    private class CargarResultLoginTask implements Runnable {
    	private ResultLogin resultLogin;
    	public CargarResultLoginTask(ResultLogin resultLogin) {
    		this.resultLogin = resultLogin;
		}
		@Override
		public void run() {
			cargarResultLogin(resultLogin);
		}
    }
    
	public void mostrarPopupAds() {
		
		LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);  
	    View popupView = layoutInflater.inflate(R.layout.popup_ads, null);  
	    popupWindow = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);  
	    
    	//mostramos el ad
	    relampaguearAdview();
    	
	    //text field con el texto de pago 
	    //de licencia recibido por parametro
	    TextView mensajeTxtView = (TextView)popupView.findViewById(R.id.mensajeSimplePopupTxtView);
	    mensajeTxtView.setText(getResources().getString(R.string.mensajeHabilitarThemes));
	    
	    android.widget.LinearLayout.LayoutParams layoutParams = new android.widget.LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	    layoutParams.gravity = Gravity.CENTER;
	    
	    //creamos el layout contenedor de
	    //las imagenes de cada tema
	    LinearLayout themesCubeImagesLinearLayout = new LinearLayout(getApplicationContext());
	    themesCubeImagesLinearLayout.setLayoutParams(layoutParams);
	    
	    //boton para salir del popup
	    Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
	    btnDismiss.setOnClickListener(new Button.OnClickListener(){
	    	@Override
	    	public void onClick(View v) {
		    	detenerRelampaguearAdmobView();
	    		//cerramos el popup
	    		popupWindow.dismiss();
	    }});
	    
	    //mostramos el popup en la vista actual
	    popupWindow.showAtLocation(findViewById(R.id.mainmenu_pager), Gravity.CENTER, 0, 0);
	    
	}
	
	private void relampaguearAdview() {
		DisplayMetrics outMetrics = getResources().getDisplayMetrics(); 
		int SCREEN_WIDTH = outMetrics.widthPixels;
		
		LinearLayout redArrowLayout = (LinearLayout) findViewById(R.id.arrowsContainer);
		redArrowLayout.setVisibility(View.VISIBLE);
		ImageView redArrow = (ImageView) redArrowLayout.getChildAt(0);
		
		TranslateAnimation translateAnimation = new TranslateAnimation(0, SCREEN_WIDTH/8, 0, 0);
		translateAnimation.setRepeatCount(Animation.INFINITE);
		translateAnimation.setRepeatMode(Animation.REVERSE);
		translateAnimation.setFillAfter(true);
		translateAnimation.setDuration(400);
		
		ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1.3f, 1f, 1f);
		scaleAnimation.setFillAfter(true);
		scaleAnimation.setDuration(400);
		scaleAnimation.setRepeatCount(Animation.INFINITE);
		scaleAnimation.setRepeatMode(Animation.REVERSE);
		
		AnimationSet animationSet = new AnimationSet(true);
		animationSet.setInterpolator(new LinearInterpolator());
		animationSet.addAnimation(translateAnimation);
		animationSet.addAnimation(scaleAnimation);
		
		redArrow.startAnimation(animationSet);
	}
	
	private void volverSplash() {
		//This method will be executed once the timer is over
        //Start your app main activity
		Intent i = new Intent(MenuPrincipalActivity.this, SplashScreenActivity.class);
        startActivity(i);
	}
	
	private void detenerRelampaguearAdmobView() {
		LinearLayout redArrowLayout = (LinearLayout) findViewById(R.id.arrowsContainer);
		redArrowLayout.setVisibility(View.GONE);
	}
	
	public void recargarMisWallpapers() {
		ViewMisDescargas viewMisDescargas = (ViewMisDescargas) menuPrincipalPagerAdapter.obtenerVista(MenuPrincipalPagerAdapter.VIEW_MISDESCARGAS);
		viewMisDescargas.cargarMisWallpapers();
	}
	
	public class NetworkChangeReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(final Context context, final Intent intent) {

			System.out.println("Receieved notification about network status");
			isNetworkAvailable(context);

		}

		private boolean isNetworkAvailable(Context context) {
			ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							if (!isConnected) {
								System.out.println("Now you are connected to Internet!");
								
								isConnected = true;
								// do your processing here ---
								
								//obtenemos vista de categorias
								MenuPrincipalPagerAdapter pagerAdapter = (MenuPrincipalPagerAdapter)obtenerPagerAdapter();
								ViewCategorias viewCategorias = (ViewCategorias) pagerAdapter.obtenerVista(MenuPrincipalPagerAdapter.VIEW_CATEGORIAS);
								if(viewCategorias.adapterData == null || viewCategorias.adapterData.size() == 0) {
									//si no hay categorias cargadas
									//volvemos al splash para forzar
									//el login con la carga inicial
									volverSplash();
								}
							}
							return true;
						}
					}
				}
			}
			System.out.println("You are not connected to Internet!");
			isConnected = false;
			return false;
		}
	}
}