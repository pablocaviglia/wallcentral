package com.gmi.wallcentralclientbase.ui.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gmi.wallcentralclientbase.MenuPrincipalActivity;
import com.gmi.wallcentralclientbase.R;
import com.gmi.wallcentralclientbase.App;
import com.gmi.wallcentralclientbase.domain.ResultObtenerWallpapers;
import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.listener.LogicFacadeListener;
import com.gmi.wallcentralclientbase.logic.LogicFacade;
import com.gmi.wallcentralclientbase.ui.view.LoadMoreListView.OnLoadMoreListener;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

public class ViewBuscar extends GenericView implements LogicFacadeListener, OnLoadMoreListener {
	
	//parametros busqueda
	private String searchKeywords;
	private String searchCategoria;
	private boolean searchPopular;
	
	public ViewBuscar(Activity activity, Bundle savedInstanceState) {
		super(R.layout.view_buscar, activity, savedInstanceState);
	}

	@Override
	public void onCreateView(LayoutInflater inflater, Bundle savedInstanceState) {
		
		//listview
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.thumblistListView);
		
		//lista contenedora de wallpapers
		List<Wallpaper> wallpapers = new ArrayList<Wallpaper>();
		
		//creamos el adapter y seteamos el contenido
		ImageAdapter imageAdapter = new ImageAdapter();
		
		//recuperamos el estado del listview 
    	if(savedInstanceState != null) {
    		
    		//obtenemos el contenido de la lista
    		wallpapers = savedInstanceState.getParcelableArrayList("listViewContent");
            searchKeywords = savedInstanceState.getString("searchKeywords");
            searchCategoria = savedInstanceState.getString("searchCategoria");
            searchPopular = savedInstanceState.getBoolean("searchPopular");
            int searchTotal = savedInstanceState.getInt("searchTotal");
            
            //obtenemos el listview
    		Parcelable listViewState = savedInstanceState.getParcelable("listView");
            
            // restore index and position
            listView.onRestoreInstanceState(listViewState);
            listView.maxItemCount = searchTotal;
    		
    		//seteamos las imagenes al adapter
    		listView.setAdapter(imageAdapter);
    		imageAdapter.setImagenes(wallpapers);
    		
    	}
    	else {
    		//seteamos el adapter a la lista
        	listView.setAdapter(imageAdapter);
    	}
    	
    	//obtenemos el tamano de la pantalla
    	int screenSize = activity.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
    	
		//cantidad de columnas dependiendo
		//del tamano de la pantalla
		if(screenSize == Configuration.SCREENLAYOUT_SIZE_SMALL || screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			listView.setNumColumns(MenuPrincipalActivity.CANTIDAD_COLUMNAS_SMALL_MEDIUM);
		}
		else {
			listView.setNumColumns(MenuPrincipalActivity.CANTIDAD_COLUMNAS_BIG);
		}
		
		//listener que escucha cuando se
    	//selecciona un elemento de la lista
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				((MenuPrincipalActivity)activity).mostrarVistaPreview((Wallpaper)view.getTag());
			}
		});
		
		//listener que escucha cuando el scroll
		//se aproxima al final para asi cargar
		//mas elementos
		listView.setOnLoadMoreListener(this);
		
		//listener que escucha el scroll
		listView.setOnScrollListener(new PauseOnScrollListener(App.imageLoader, MenuPrincipalActivity.PAUSE_ONSCROLL, MenuPrincipalActivity.PAUSE_ONFLING));
		
	}
	
    public void iniciarBusqueda(String keywords, String categoria, boolean popular) {
		
		//guardamos criterios de busqueda
		searchKeywords = keywords;
		searchCategoria = categoria;
		searchPopular = popular;
		
		//obtenemos el imageadapter
		//para sacarle todo el contenido
		obtenerImageAdapter().vaciarImagenes();
		
		//mostramos el circulo de carga
		mostrarLoadingCircle();

		//comenzamos nueva busqueda
		obtenerWallpapers();
		
		//ocultamos el teclado
		ocultarTeclado();

    }
    
    private ImageAdapter obtenerImageAdapter() {
		//limpiamos busqueda previa
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.thumblistListView);
		ImageAdapter imageAdapter = (ImageAdapter) listView.getAdapter();
		return imageAdapter;
    }
	
    @Override
    public void onSaveInstanceState(Bundle outState) {
        
		//listview
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.thumblistListView);
		
        //salvamos el listview
        Parcelable listViewState = listView.onSaveInstanceState();
        outState.putParcelable("listView", listViewState);
        
        //salvamos el contenido 
        outState.putParcelableArrayList("listViewContent", obtenerImageAdapter().obtenerImagenes());
        outState.putString("searchKeywords", searchKeywords);
        outState.putString("searchCategoria", searchCategoria);
        outState.putBoolean("searchPopular", searchPopular);
        outState.putInt("searchTotal", listView.maxItemCount);
        
    }
    
	private ResultObtenerWallpapers obtenerWallpapers() {
		mostrarLoadingCircle();
		System.out.println("Getting more wallpapers...");
    	ResultObtenerWallpapers resultObtenerWallpapers = LogicFacade.search(searchKeywords, searchCategoria, searchPopular, obtenerImageAdapter().obtenerImagenes().size(), this);
    	return resultObtenerWallpapers;
    }
	
	public void cargarWallpapers(int total, String categoria, String keywords, Boolean popular, List<Wallpaper> wallpapers) {
		this.searchPopular = popular;
		this.searchCategoria = categoria;
		this.searchKeywords = keywords;
		cargarWallpapers(total, wallpapers, true);		
	}
	
	@Override
	public void operationFinished(Object result, byte operationType) {
		
		if(operationType == LogicFacadeListener.OPERATION_ObtenerWallpapers) {
			ocultarLoadingCircle();
			ResultObtenerWallpapers resultObtenerWallpapers = (ResultObtenerWallpapers)result;
			
			if(resultObtenerWallpapers != null && !resultObtenerWallpapers.error) {
				cargarWallpapers(resultObtenerWallpapers.total, resultObtenerWallpapers.wallpapers, false);
			}
			else {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(view != null) {
							Toast.makeText(view.getContext(), activity.getResources().getString(R.string.search_wallpapers_error), Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		}
	}
	
	public void cargarWallpapers(final int total, final List<Wallpaper> wallpapersToAdd, final boolean clear) {
		
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				
				//listview
				LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.thumblistListView);
				listView.maxItemCount = total;
				
	            //call onLoadMoreComplete when the LoadMore task, has finished
	            listView.onLoadMoreComplete();
	            
				if(clear) {
					obtenerImageAdapter().setImagenes(wallpapersToAdd);
				}
				else {
					obtenerImageAdapter().addImagenes(wallpapersToAdd);
				}
			}
		});
	}
	
	private void ocultarTeclado() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
			    Context context = view.getContext();
			    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		});
	}
	
	private void mostrarLoadingCircle() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ProgressBar progressBar = (ProgressBar) ViewBuscar.this.view.findViewById(R.id.thumblistProgressBar);
				progressBar.setVisibility(View.VISIBLE);
			}
		});
	}
	
	private void ocultarLoadingCircle() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ProgressBar progressBar = (ProgressBar) ViewBuscar.this.view.findViewById(R.id.thumblistProgressBar);
				progressBar.setVisibility(View.GONE);
			}
		});
	}
	
	private class ImageAdapter extends BaseAdapter {
		
		private Vector<Wallpaper> imagenes = new Vector<Wallpaper>();
		
		public ArrayList<Wallpaper> obtenerImagenes() {
			return new ArrayList<Wallpaper>(imagenes);
		}
		
		public void vaciarImagenes() {
			this.imagenes.clear();
			notifyDataSetChanged();
		}
		
    	public void setImagenes(List<Wallpaper> imagenes) {
    		this.imagenes.clear();
    		addImagenes(imagenes);
    	}
    	
    	public void addImagenes(List<Wallpaper> imagenes) {
    		this.imagenes.addAll(imagenes);
    		notifyDataSetChanged();
    	}

		@Override
		public int getCount() {
			return imagenes.size();
		}
		@Override
		public Object getItem(int position) {
			return null;
		}
		@Override
		public long getItemId(int position) {
			return position;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			//obtenemos la vista donde va
			//a ser mostrada la imagen
			final ImageView imageView;
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				imageView = (ImageView) inflater.inflate(R.layout.item_grid_image, parent, false);
			} else {
				imageView = (ImageView) convertView;
			}
			
			//obtenemos el wallpaper correspondiente
			//a la posicion siendo vista en la lista
			final Wallpaper wallpaper = imagenes.get(position);
			wallpaper.downloadThumbRetry = 0;
			imageView.setTag(wallpaper);
			
			App.imageLoader.displayImage(
					wallpaper.obtenerUrlThumb(), 
					imageView, 
					App.options,
					new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String arg0, View arg1) {
						}
						@Override
						public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
							//politica de reintento
							if(wallpaper.downloadThumbRetry < 1) {
								//intentamos recargar
								App.imageLoader.displayImage(
										wallpaper.obtenerUrlThumb(), 
										imageView, 
										App.options);
							}
							wallpaper.downloadThumbRetry++;
						}
						@Override
						public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
						}
						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
						}
					});
			
			return imageView;
		}
	}
	
    /**
     * Do the work to load more items at the end of list here
     */
    public void onLoadMore() {
        obtenerWallpapers();
    }
}