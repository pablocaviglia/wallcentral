package com.gmi.wallcentralclientbase.ui.view;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.gmi.wallcentralclientbase.R;
import com.gmi.wallcentralclientbase.domain.MetadataListData;
import com.gmi.wallcentralclientbase.domain.ResultMetadata;
import com.gmi.wallcentralclientbase.ui.adapter.MetadataListAdapter;

public abstract class GenericView {

	protected View view;
	protected Activity activity;

	/**
	 * Metodos abstractos
	 */
    public abstract void onCreateView(LayoutInflater inflater, Bundle savedInstanceState);
    public abstract void onSaveInstanceState(Bundle outState);

    /**
     * Implementacion
     */
	public GenericView(int layoutId, Activity activity, Bundle savedInstanceState) {
		LayoutInflater inflater = (LayoutInflater)activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.activity = activity;
		view = inflater.inflate(layoutId, null);
		onCreateView(inflater, savedInstanceState);
	}
	
	public View getView(Context context) {
		return view;
	}
	
	protected void runOnUiThread(Runnable runnable) {
		Handler mHandler = new Handler(Looper.getMainLooper());
		mHandler.post(runnable);
	}
	
	protected class CargarMetadataTask implements Runnable {
		private ResultMetadata resultMetadata;
		private ArrayList<MetadataListData> itemDetailsArrayList;
		private View view;
		public CargarMetadataTask(View view, ResultMetadata resultMetadata) {
			this.view = view;
			this.resultMetadata = resultMetadata;
		}
		@Override
		public void run() {
			
			itemDetailsArrayList = new ArrayList<MetadataListData>();
			itemDetailsArrayList.add(new MetadataListData(activity.getResources().getString(R.string.name), resultMetadata.name));
			itemDetailsArrayList.add(new MetadataListData(activity.getResources().getString(R.string.category), resultMetadata.category));
			itemDetailsArrayList.add(new MetadataListData(activity.getResources().getString(R.string.downloads), resultMetadata.downloads != null ? resultMetadata.downloads.toString() : ""));
			itemDetailsArrayList.add(new MetadataListData(activity.getResources().getString(R.string.keywords), resultMetadata.keywords));
			
			ListView listView = (ListView) view.findViewById(R.id.metadataListView);
			listView.setAdapter(new MetadataListAdapter(itemDetailsArrayList, activity.getApplicationContext()));
			
		}
	}
	
	protected class OcultarTecladoTask implements Runnable {
		private View view;
		public OcultarTecladoTask(View view) {
			this.view = view;
		}
		@Override
		public void run() {
		    Context context = view.getContext();
		    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}
	
	protected class MostrarLoadingCircleTask implements Runnable {
		private ProgressBar progressBar;
		public MostrarLoadingCircleTask(ProgressBar progressBar) {
			this.progressBar = progressBar;
		}
		@Override
		public void run() {
			progressBar.setVisibility(View.VISIBLE);
		}
	}
	
	
	protected class OcultarLoadingCircleTask implements Runnable {
		private ProgressBar progressBar;
		public OcultarLoadingCircleTask(ProgressBar progressBar) {
			this.progressBar = progressBar;
		}
		@Override
		public void run() {
			progressBar.setVisibility(View.GONE);
		}
	}
}