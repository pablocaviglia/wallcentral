package com.gmi.wallcentralclientbase.ui.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.gmi.wallcentralclientbase.R;
import com.gmi.wallcentralclientbase.domain.ResultMetadata;
import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.listener.LogicFacadeListener;
import com.gmi.wallcentralclientbase.logic.LogicFacade;

public class ViewMetadata extends GenericView implements LogicFacadeListener {
	
	private CargarMetadataTask cargarMetadataTask;
	
	public ViewMetadata(Activity activity, Bundle savedInstanceState) {
		super(R.layout.view_metadata, activity, savedInstanceState);
	}
	
	@Override
	public void onCreateView(LayoutInflater inflater, Bundle savedInstanceState) {
		
	}
	
    public void obtenerMetadata(Wallpaper wallpaper) {
    	LogicFacade.obtenerMetadata(wallpaper.id, this);
    }
    
	
	private void cargarMetadata(ResultMetadata resultMetadata) {
		cargarMetadataTask = new CargarMetadataTask(view, resultMetadata);
		runOnUiThread(cargarMetadataTask);
	}
	
	@Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
		
	}

	@Override
	public void operationFinished(Object result, byte operationType) {
		ResultMetadata resultMetadata = (ResultMetadata) result;
		cargarMetadata(resultMetadata);
	}
}