package com.gmi.wallcentralclientbase.ui.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.gmi.wallcentralclientbase.App;
import com.gmi.wallcentralclientbase.FullscreenImageActivity;
import com.gmi.wallcentralclientbase.MenuPrincipalActivity;
import com.gmi.wallcentralclientbase.R;
import com.gmi.wallcentralclientbase.db.PersistenceHelper;
import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.util.DownloadWallpaperIf;
import com.gmi.wallcentralclientbase.util.DownloadWallpaperTask;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ViewAnticipo extends GenericView implements DownloadWallpaperIf {
	
	//runnables
	private OcultarTecladoTask ocultarTecladoTask;
	public DownloadWallpaperTask downloadWallpaperTask;
	private boolean descargando;
	private Wallpaper wallpaper;
	
	public ViewAnticipo(Activity activity, Bundle savedInstanceState) {
		super(R.layout.view_anticipo, activity, savedInstanceState);
	}
	
	@Override
	public void onCreateView(LayoutInflater inflater, Bundle savedInstanceState) {
	
	}
	
	public void descargarWallpaper() {
		if(!wallpaper.obtenerArchivo().exists()) {
			descargarWallpaper(wallpaper);	
		}
	}
	
	public void iniciarPreviewImagen(final Wallpaper wallpaper) {
		
		this.wallpaper = wallpaper;
		
        //sacamos la visibility de la imagen grande
        ((ImageView) view.findViewById(R.id.imgBigSize)).setVisibility(View.GONE);
	    
		//si el archivo ya existe cargamos
		//la imagen en alta definicion, sino
		//solamente cargamos el thumb
		String urlImagen = null;
		
		//obtenemos la vista donde 
		//va a ser desplegada la imagen
        ImageView imageView = null;
		
		if(wallpaper.obtenerArchivo().exists()) {
			urlImagen = "file://" + wallpaper.obtenerArchivo().getPath();
			imageView = (ImageView) view.findViewById(R.id.imgBigSize);
			imageView.setVisibility(View.VISIBLE);
	        ((ImageView) view.findViewById(R.id.imgPreview)).setVisibility(View.GONE);
		}
		else {
			urlImagen = wallpaper.obtenerUrlThumb();
			imageView = (ImageView) view.findViewById(R.id.imgPreview);
			imageView.setVisibility(View.VISIBLE);
			((ImageView) view.findViewById(R.id.imgBigSize)).setVisibility(View.GONE);
		}

		//cargamos la imagen
        App.imageLoader.displayImage(urlImagen, imageView, App.options);
        
        //refrescamos la vista y pedimos
        //request de la misma
        imageView.refreshDrawableState();
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        
        //carga la botonera
		cargarBotonera(wallpaper);
		
		//a veces aparece el teclado
        //forzamos a esconderlo
		ocultarTeclado();
		
		//click listener
		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupWindow popupWindow = ((MenuPrincipalActivity)activity).popupWindow;
				if(popupWindow != null && !popupWindow.isShowing() || popupWindow == null) {
					Intent i = new Intent(activity.getApplicationContext(), FullscreenImageActivity.class);
					i.putExtra(App.BUNDLE_WALLPAPER, (Serializable)wallpaper);
			        activity.startActivity(i);
				}
			}
		});
	}
	
	private void ocultarTeclado() {
		runOnUiThread(ocultarTecladoTask);
	}
	
	private void cargarBotonera(final Wallpaper wallpaper) {
		
        //botonera
        if(!wallpaper.obtenerArchivo().exists()) {
        	//cambiamos estado de botones
        	((Button)view.findViewById(R.id.deleteBtn)).setVisibility(View.GONE);
        	((Button)view.findViewById(R.id.ponerBtn)).setVisibility(View.GONE);
        	
			//habilitamos el boton para descargar
            final Button downloadButton = (Button)view.findViewById(R.id.downloadBtn);
            //seteamos el texto del boton
            downloadButton.setText(activity.getResources().getString(R.string.download));
            
            downloadButton.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				//descargamos wallpaper
    				descargarWallpaper(wallpaper);   				
    			}
    		});
            downloadButton.setVisibility(View.VISIBLE);
        	
        }
        else {
        	
        	//deshabilitamos el boton de bajar
        	((Button)view.findViewById(R.id.downloadBtn)).setVisibility(View.GONE);
        	
        	//habilitamos el boton de delete
        	Button deleteButton = (Button)view.findViewById(R.id.deleteBtn);
        	deleteButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//borramos el archivo
					if(wallpaper.obtenerArchivo().exists()) {
						wallpaper.obtenerArchivo().delete();
					}
					
					MenuPrincipalActivity menuPrincipalActivity = (MenuPrincipalActivity) activity;
					
					//recargamos el listado de 
					//wallpapers descargados
					menuPrincipalActivity.recargarMisWallpapers();
					
					//volvemos al menu de busqueda
					menuPrincipalActivity.mostrarVistaMenuPrincipal(menuPrincipalActivity.tabMenuActual);
				}
			});
        	deleteButton.setVisibility(View.VISIBLE);
        	
        	//boton de poner
            Button setWallpaperButton = (Button)view.findViewById(R.id.ponerBtn);
            setWallpaperButton.setOnClickListener(new OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				try {
        				//seteamos el wallpaper
        				WallpaperManager wm = WallpaperManager.getInstance(activity);
        				wm.setStream(new FileInputStream(wallpaper.obtenerArchivo()));
        				
        				//mensaje indicando que 
        				//el wallpaper fue seteado
        				Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.set_wallpaper_desktop), Toast.LENGTH_SHORT).show();
					} 
    				catch (Exception e) {
						e.printStackTrace();
					}
    			}
    		});
            setWallpaperButton.setVisibility(View.VISIBLE);
        }
	}
	
	public void descargarWallpaper(Wallpaper wallpaper) {
		
		if(!descargando) {
			descargando = true;
			
			//obtenemos la cantidad de wallpapers
			//descargados sin haber cliqueado el ad
			int wallpapersDescargadosSinCliquearAd = Integer.valueOf(App.data.obtenerValorConfig(PersistenceHelper.CONFIG_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD));
			if(wallpapersDescargadosSinCliquearAd > App.MAXIMO_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD) {
				descargando = false;
				((MenuPrincipalActivity)activity).mostrarPopupAds();
			}
			else {
				
				//seteamos el texto del boton
				((Button)view.findViewById(R.id.downloadBtn)).setText(activity.getResources().getString(R.string.downloading));
				
				//guardamos el wallpaper
				App.data.guardarWallpaper(wallpaper);
				
				//instanciamos la tarea 
				//encargada de bajar las imagenes
				downloadWallpaperTask = new DownloadWallpaperTask(view, this);
				
				//comenzamos la descarga
				downloadWallpaperTask.execute(wallpaper);
			}
		}
	}
	
	@Override
	public void downloadFinished(final Wallpaper wallpaper, byte status, Object result) {
		
		//download ok
		if(status == DownloadWallpaperIf.STATUS_DOWNLOADED) {
			
			//incrementamos la cantidad de wallpapers
			//descargados sin mostrar el rectangle y 
			//la pantalla de rating
			App.data.incrementarConfigWallpapersDescargadosSinCliquearAd();
			App.data.decrementarConfigCalificaAplicacion();
			
			//archivo creado mientras
			//se bajaba el archivo
			File file = (File)result;
			
			if(file.exists()) {
				
				//si el archivo fue descargado correctamente
				wallpaper.archivoDirecto = file.getAbsolutePath();
				wallpaper.fileLastMod = file.lastModified();
				
				//cargamos la nueva 
				//imagen de alta calidad
				final ImageView imageBigSizeView = (ImageView) view.findViewById(R.id.imgBigSize);
		        App.imageLoader.displayImage(
		        		"file://" + file.getPath(), 
		        		imageBigSizeView, 
		        		App.options,
		        		new ImageLoadingListener() {
							@Override
							public void onLoadingStarted(String arg0, View arg1) {
							}
							@Override
							public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
							}
							@Override
							public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
								imageBigSizeView.setVisibility(View.VISIBLE);
								cargarBotonera(wallpaper);
							}
							@Override
							public void onLoadingCancelled(String arg0, View arg1) {
							}
						});
			}
			
			MenuPrincipalActivity menuPrincipalActivity = (MenuPrincipalActivity)activity;
			
			//recargamos el listado de 
			//wallpapers descargados
			menuPrincipalActivity.recargarMisWallpapers();
			
			//evaluamos si mostrar el cartel
			//que pide la evaluacion de la 
			//aplicacion en google play
			menuPrincipalActivity.evaluarMostrarPopupRating();
			
		}
		else if(status == DownloadWallpaperIf.STATUS_CANCELLED || 
				 status == DownloadWallpaperIf.STATUS_ERROR) {
			
			if(status == DownloadWallpaperIf.STATUS_ERROR) {
				//mensaje indicando que el  
				//wallpaper no pudo ser descargado
				Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.download_error), Toast.LENGTH_SHORT).show();
			}
			
			//recargamos la botonera
            cargarBotonera(wallpaper);
            
		}
		
		descargando = false;
	}
	
	@Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
		
	}
}