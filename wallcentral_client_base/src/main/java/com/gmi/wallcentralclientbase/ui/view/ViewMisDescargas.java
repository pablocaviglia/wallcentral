package com.gmi.wallcentralclientbase.ui.view;

import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gmi.wallcentralclientbase.MenuPrincipalActivity;
import com.gmi.wallcentralclientbase.R;
import com.gmi.wallcentralclientbase.App;
import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.logic.LogicFacade;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

public class ViewMisDescargas extends GenericView {
	
	public ViewMisDescargas(Activity activity, Bundle savedInstanceState) {
		super(R.layout.view_misdescargas, activity, savedInstanceState);
	}

	@Override
	public void onCreateView(LayoutInflater inflater, Bundle savedInstanceState) {
		
		//create the adapter
		ImageAdapter imageAdapter = new ImageAdapter();
		
		//listview
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.misWallpapersListView);

    	//obtenemos el tamano de la pantalla
    	int screenSize = activity.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
    	
		//cantidad de columnas dependiendo
		//del tamano de la pantalla
		if(screenSize == Configuration.SCREENLAYOUT_SIZE_SMALL || screenSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			listView.setNumColumns(MenuPrincipalActivity.CANTIDAD_COLUMNAS_SMALL_MEDIUM);
		}
		else {
			listView.setNumColumns(MenuPrincipalActivity.CANTIDAD_COLUMNAS_BIG);
		}
		
		listView.setAdapter(imageAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				((MenuPrincipalActivity)activity).mostrarVistaPreview((Wallpaper)view.getTag());
			}
		});
		
		//listener que escucha el scroll
		listView.setOnScrollListener(new PauseOnScrollListener(App.imageLoader, MenuPrincipalActivity.PAUSE_ONSCROLL, MenuPrincipalActivity.PAUSE_ONFLING));
		
		//recuperamos el estado del listview 
    	if(savedInstanceState != null) {
            //obtenemos el listview
    		Parcelable listViewState = savedInstanceState.getParcelable("listView");
            listView.onRestoreInstanceState(listViewState);
    	}		
    	
    	//cargamos los wallpapers
    	cargarMisWallpapers();
    	
	}
	
	public void cargarMisWallpapers() {
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.misWallpapersListView);
		ImageAdapter imageAdapter = (ImageAdapter) listView.getAdapter();
		imageAdapter.setImagenes(LogicFacade.obtenerWallpapersLocales());
		imageAdapter.notifyDataSetChanged();
		
	}
	
	public void notificarCambioContenido() {
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.misWallpapersListView);
		ImageAdapter imageAdapter = (ImageAdapter) listView.getAdapter();
		imageAdapter.notifyDataSetChanged();
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
		
		//obtenemos la lista
		LoadMoreListView listView = (LoadMoreListView) view.findViewById(R.id.misWallpapersListView);

		//la salvamos
        Parcelable listViewState = listView.onSaveInstanceState();
        outState.putParcelable("listView", listViewState);
	}
	
    private class ImageAdapter extends BaseAdapter {
    	
    	private Vector<Wallpaper> imagenes = new Vector<Wallpaper>();
    	
    	public void setImagenes(List<Wallpaper> imagenes) {
    		this.imagenes.clear();
    		this.imagenes.addAll(imagenes);
    	}
    	
    	@Override
    	public int getCount() {
    		return imagenes.size();
    	}
    	@Override
    	public Object getItem(int position) {
    		return null;
    	}
    	@Override
    	public long getItemId(int position) {
    		return position;
    	}
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		final ImageView imageView;
    		if (convertView == null) {
    			imageView = (ImageView) activity.getLayoutInflater().inflate(R.layout.item_grid_image, parent, false);
    		} else {
    			imageView = (ImageView) convertView;
    		}
    		
    		Wallpaper wallpaper = imagenes.get(position);
    		imageView.setTag(wallpaper);
    		App.imageLoader.displayImage("file://" + wallpaper.archivoDirecto, imageView, App.options);
    		return imageView;
    	}
    }
}