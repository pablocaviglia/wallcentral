package com.gmi.wallcentralclientbase.ui.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gmi.wallcentralclientbase.MenuPrincipalActivity;
import com.gmi.wallcentralclientbase.R;

public class ViewCategorias extends GenericView {
	
	private ListView categoriasListView;
	private ArrayAdapter<String> adapter;
	public ArrayList<String> adapterData;
	
	public ViewCategorias(Activity activity, Bundle savedInstanceState) {
		super(R.layout.view_categorias, activity, savedInstanceState);
	}
	
	@Override
	public void onCreateView(LayoutInflater inflater, Bundle savedInstanceState) {
    	
    	//obtenemos la lista
		categoriasListView = (ListView) view.findViewById(R.id.categoriasListView);
		adapterData = new ArrayList<String>();
		adapter = new ArrayAdapter<String>(activity, R.layout.list_textview, adapterData);
		categoriasListView.setAdapter(adapter);
		
		categoriasListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				//obtenemos la categoria elegida
				String categoriaElegida = adapter.getItem(position);
				
				//iniciamos la busqueda
				((MenuPrincipalActivity)activity).iniciarBusqueda(null, categoriaElegida, true);
				
			}
		});
		
    	if(savedInstanceState != null) {
    		adapterData = savedInstanceState.getStringArrayList("adapterData");
    		cargarCategorias(adapterData);
    	}
	 }
	
	public void cargarCategorias(List<String> categorias) {
		adapter.clear();
		for(String categoria : categorias) {
			adapter.add(categoria);	
		}
		
		//agregamos una categoria all
		adapter.add("");
		adapter.notifyDataSetChanged();
	}
	
	@Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putStringArrayList("adapterData", adapterData);
	}
}