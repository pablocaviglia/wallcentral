package com.gmi.wallcentralclientbase.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gmi.wallcentralclientbase.R;
import com.gmi.wallcentralclientbase.domain.MetadataListData;

public class MetadataListAdapter extends BaseAdapter {

	private static ArrayList<MetadataListData> itemDetailsArrayList;
	private Context context;

	public MetadataListAdapter(ArrayList<MetadataListData> result, Context c) {
		itemDetailsArrayList = result;
		context = c;
	}
	
	@Override
	public int getCount() {
		return itemDetailsArrayList.size();
	}
	
	@Override
	public Object getItem(int position) {
		return itemDetailsArrayList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater layoutInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = layoutInflator.inflate(R.layout.list_metadata_layout, parent, false);

		TextView titleTextview = (TextView) row.findViewById(R.id.metadata_title);
		TextView valueTextview = (TextView) row.findViewById(R.id.metadata_value);
		
		titleTextview.setText(itemDetailsArrayList.get(position).getTitle());
		valueTextview.setText(itemDetailsArrayList.get(position).getValue());

		return (row);
	}
}