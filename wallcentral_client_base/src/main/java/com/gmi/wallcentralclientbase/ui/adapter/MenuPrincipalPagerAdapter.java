package com.gmi.wallcentralclientbase.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;

import com.gmi.wallcentralclientbase.ui.view.GenericView;
import com.gmi.wallcentralclientbase.ui.view.ViewBuscar;
import com.gmi.wallcentralclientbase.ui.view.ViewCategorias;
import com.gmi.wallcentralclientbase.ui.view.ViewMisDescargas;
import com.gmi.wallcentralclientbase.ui.view.ViewPopular;

public class MenuPrincipalPagerAdapter extends PagerAdapter {
	
    //constantes
    public static final int VIEW_POPULAR = 0;
    public static final int VIEW_BUSCAR = 1;
    public static final int VIEW_MISDESCARGAS = 2;
    public static final int VIEW_CATEGORIAS = 3;
    
	private List<GenericView> views;
	
	public MenuPrincipalPagerAdapter(Activity activity, Bundle savedInstanceState) {
		views = new ArrayList<GenericView>();
		views.add(VIEW_POPULAR, new ViewPopular(activity, savedInstanceState));
		views.add(VIEW_BUSCAR, new ViewBuscar(activity, savedInstanceState));
		views.add(VIEW_MISDESCARGAS, new ViewMisDescargas(activity, savedInstanceState));
		views.add(VIEW_CATEGORIAS, new ViewCategorias(activity, savedInstanceState));
	}
	
	public GenericView obtenerVista(int position) {
		return views.get(position);
	}
	
	public void onSaveInstanceState(Bundle outState) {
		for(GenericView genericView : views) {
			genericView.onSaveInstanceState(outState);
		}
	}

	public int getCount() {
		return views.size();
	}
	
	public Object instantiateItem(View collection, int position) {
		View view = views.get(position).getView(collection.getContext());
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		((ViewPager) collection).addView(view, 0);
		return view;
	}
	
	@Override
	public void destroyItem(View arg0, int arg1, Object view) {
		((ViewPager) arg0).removeView((View) view);
	}
	
	@Override
	public boolean isViewFromObject(View arg0, Object view) {
		return arg0 == ((View) view);
	}
	
	@Override
	public Parcelable saveState() {
		return null;
	}
}