package com.gmi.wallcentralclientbase.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gmi.wallcentralclientbase.App;
import com.gmi.wallcentralclientbase.domain.ResultLogin;
import com.gmi.wallcentralclientbase.domain.ResultMetadata;
import com.gmi.wallcentralclientbase.domain.ResultObtenerWallpapers;
import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.listener.LogicFacadeListener;

public class LogicFacade {
	
	public static void login(final LogicFacadeListener listener) {
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				ResultLogin resultLogin = new ResultLogin();
				
				try {
					JSONObject loginJSON = obtenerLoginJSON();
					if(loginJSON != null) {
						
						//parseamos resultado
						Long loginTimestamp = loginJSON.getLong("loginTimestamp");
						String defaultSearch = loginJSON.getString("defaultSearch");
						JSONObject obtenerWallpapersJSON = loginJSON.getJSONObject("defaultSearchResult");
						JSONArray categoriesJSON = loginJSON.getJSONArray("categories");
						
						int categoriesLength = categoriesJSON.length();
						List<String> categories = new ArrayList<String>();
						for(int i=0; i<categoriesLength; i++) {
							categories.add(categoriesJSON.getString(i));
						}
						
						//ordenamos lexicograficamente
						Collections.sort(categories);
						
						Long total = obtenerWallpapersJSON.getLong("total");
						JSONArray elementsJSONArray = obtenerWallpapersJSON.getJSONArray("elements");
						int elementsLength = elementsJSONArray.length();
						
						ArrayList<Wallpaper> wallpapersObtenidos = new ArrayList<Wallpaper>();
						for(int i=0; i<elementsLength; i++) {
							JSONObject wallpaperJSONObject = elementsJSONArray.getJSONObject(i);
							Long wallpaperId = wallpaperJSONObject.getLong("id");
							String wallpaperName = wallpaperJSONObject.getString("name");
							String wallpaperKey = wallpaperJSONObject.getString("key");
							String wallpaperPath = wallpaperJSONObject.getString("path");
							wallpapersObtenidos.add(new Wallpaper(wallpaperId.intValue(), wallpaperName, wallpaperPath, wallpaperKey));
						}
						
						//llenamos el objeto que contiene
						//el resultado devuelto por el server
						ResultObtenerWallpapers resultObtenerWallpapers = new ResultObtenerWallpapers();
						resultObtenerWallpapers.total = total.intValue();
						resultObtenerWallpapers.wallpapers = wallpapersObtenidos;
						
						resultLogin = new ResultLogin();
						resultLogin.defaultSearch = defaultSearch;
						resultLogin.loginTimestamp = new Timestamp(loginTimestamp);
						resultLogin.defaultSearchResult = resultObtenerWallpapers;
						resultLogin.categorias = categories;
						
						//llamamos al listener
						listener.operationFinished(resultLogin, LogicFacadeListener.OPERATION_Login);
					}
					else {
						System.err.println("ERROR OBTENIENDO JSON, SERVER DEVOLVIO VACIO!!!");
						//llamamos al listener indicando error
						resultLogin.error = true;
						listener.operationFinished(resultLogin, LogicFacadeListener.OPERATION_Login);
					}
				}
				catch(Exception e) {
					e.printStackTrace();
					//llamamos al listener indicando error
					resultLogin.error = true;
					listener.operationFinished(resultLogin, LogicFacadeListener.OPERATION_Login);
				}
			}
		});
		
		t.start();

	}

	public static ResultObtenerWallpapers search(final String keywords, final String categoria, final boolean popular, final int from, final LogicFacadeListener listener) {
		
		System.out.println("---> Getting Keywords: " + keywords + " Category: " + categoria + " Popular: " + popular + " from " + from);
		final ResultObtenerWallpapers resultObtenerWallpapers = new ResultObtenerWallpapers();
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {

				try {
					
					//lamada JSON
					JSONObject obtenerWallpapersJSON = obtenerWallpapersJSON(keywords, categoria, popular, from);
					
					if(obtenerWallpapersJSON != null) {
						
						//parseamos resultado
						Long total = obtenerWallpapersJSON.getLong("total");
						JSONArray elementsJSONArray = obtenerWallpapersJSON.getJSONArray("elements");
						int elementsLength = elementsJSONArray.length();
						
						ArrayList<Wallpaper> wallpapersObtenidos = new ArrayList<Wallpaper>();
						for(int i=0; i<elementsLength; i++) {
							JSONObject wallpaperJSONObject = elementsJSONArray.getJSONObject(i);
							Long wallpaperId = wallpaperJSONObject.getLong("id");
							String wallpaperName = wallpaperJSONObject.getString("name");
							String wallpaperKey = wallpaperJSONObject.getString("key");
							String wallpaperPath = wallpaperJSONObject.getString("path");
							wallpapersObtenidos.add(new Wallpaper(wallpaperId.intValue(), wallpaperName, wallpaperPath, wallpaperKey));
						}
						
						//llenamos el objeto que contiene
						//el resultado devuelto por el server
						resultObtenerWallpapers.total = total.intValue();
						resultObtenerWallpapers.wallpapers = wallpapersObtenidos;
						
						//llamamos al listener
						listener.operationFinished(resultObtenerWallpapers, LogicFacadeListener.OPERATION_ObtenerWallpapers);
					}
					else {
						//llamamos al listener
						resultObtenerWallpapers.error = true;
						listener.operationFinished(resultObtenerWallpapers, LogicFacadeListener.OPERATION_ObtenerWallpapers);
					}
				}
				catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		
		t.start();
		
		return resultObtenerWallpapers;
	}
	
	public static ResultMetadata obtenerMetadata(final int wallpaperId, final LogicFacadeListener listener) {
		
		final ResultMetadata resultMetadata = new ResultMetadata();
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {

				try {
					
					//lamada JSON
					JSONObject obtenerMetadataJSON = obtenerMetadataJSON(wallpaperId);
					
					if(obtenerMetadataJSON != null) {
						
						//parseamos resultado
						Integer id = obtenerMetadataJSON.getInt("id");
						String name = obtenerMetadataJSON.getString("name");
						String path = obtenerMetadataJSON.getString("path");
						String key = obtenerMetadataJSON.getString("key");
						Integer downloads = obtenerMetadataJSON.getInt("downloads");
						String category = obtenerMetadataJSON.getString("category");
						String keywords = obtenerMetadataJSON.getString("keywords");
						
						resultMetadata.id = id;
						resultMetadata.name = name;
						resultMetadata.path = path;
						resultMetadata.key = key;
						resultMetadata.downloads = downloads;
						resultMetadata.category = category;
						resultMetadata.keywords = keywords;
						
						//llamamos al listener
						listener.operationFinished(resultMetadata, LogicFacadeListener.OPERATION_Metadata);
					}
					else {
						listener.operationFinished(resultMetadata, LogicFacadeListener.OPERATION_Metadata);
					}
				}
				catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		
		t.start();
		
		return resultMetadata;
	}
	
	private static JSONObject obtenerLoginJSON() {
		
		try {
			
			String loginUrl = App.SERVER_APPLICATION_CONTEXT + "/action/wallpaper/login";
			if(App.CATEGORIA_BASE != null && !App.CATEGORIA_BASE.trim().equals("")) {
				loginUrl += "?category=" + App.CATEGORIA_BASE;
			}
			
			//json service
			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(loginUrl);
			
			//ejecutamos http post
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			
			if (statusCode == 200) { //result OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} 
			else { //result BAD
				System.err.println("Failed to download file");
			}
			
			//resultado JSON
			String jsonFeed = builder.toString();
			
			//creamos objeto JSON
			JSONObject jsonObject = new JSONObject(jsonFeed);
			
			return jsonObject;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static JSONObject obtenerWallpapersJSON(String keywords, String categoria, boolean popular, int from) {
		
		try {
			
			//json service
			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(App.SERVER_APPLICATION_CONTEXT + "/action/wallpaper/search");
			
			//params
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();  
			pairs.add(new BasicNameValuePair("offset", String.valueOf(from)));
			pairs.add(new BasicNameValuePair("keywords", keywords));  
			
			if(App.CATEGORIA_BASE != null && !App.CATEGORIA_BASE.trim().equals("")) {
				pairs.add(new BasicNameValuePair("category", App.CATEGORIA_BASE));
				pairs.add(new BasicNameValuePair("subcategory", categoria));
			}
			else {
				pairs.add(new BasicNameValuePair("category", categoria));
			}			
			
			pairs.add(new BasicNameValuePair("popular", String.valueOf(popular)));
			UrlEncodedFormEntity paramsEntity = new UrlEncodedFormEntity(pairs, HTTP.UTF_8);  
			httpPost.setEntity(paramsEntity);  
			
			//ejecutamos http post
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			
			if (statusCode == 200) { //result OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} 
			else { //result BAD
				System.err.println("Failed to download file");
			}
			
			//resultado JSON
			String jsonFeed = builder.toString();
			
			//creamos objeto JSON
			JSONObject jsonObject = new JSONObject(jsonFeed);
			
			return jsonObject;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static JSONObject obtenerMetadataJSON(int wallpaperId) {
		
		try {
			
			//json service
			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(App.SERVER_APPLICATION_CONTEXT + "/action/wallpaper/info");
			
			//params
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("wallpaperId", String.valueOf(wallpaperId)));
			UrlEncodedFormEntity paramsEntity = new UrlEncodedFormEntity(pairs, HTTP.UTF_8);
			httpPost.setEntity(paramsEntity);
			
			//ejecutamos http post
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			
			if (statusCode == 200) { //result OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} 
			else { //result BAD
				System.err.println("Failed to download file");
			}
			
			//resultado JSON
			String jsonFeed = builder.toString();
			
			//creamos objeto JSON
			JSONObject jsonObject = new JSONObject(jsonFeed);
			
			return jsonObject;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Wallpaper> obtenerWallpapersLocales() {
		
		//obtenemos los wallpapers 
		//de la carpeta indicada
		List<Wallpaper> wallpapers = getFolderWallpapers(App.WALLPAPERS_OUTPUT_FOLDER, null);
		
		//ordenamos por fecha de modificacion
		Collections.sort(wallpapers, new Comparator<Wallpaper>() {
			public int compare(Wallpaper w1, Wallpaper w2) {
				return (int)(w2.fileLastMod - w1.fileLastMod);
			}
		});
		
		return wallpapers;
	}
	
	public static List<Wallpaper> getFolderWallpapers(File folder, List<Wallpaper> wallpapers) {
		
		if(wallpapers == null) {
			wallpapers = new ArrayList<Wallpaper>();
		}
		
		File[] folderFiles = folder.listFiles();
		for(File currentFolderFile : folderFiles) {
			if(currentFolderFile.isFile()) {
				Wallpaper wallpaper = new Wallpaper(0, "", "", "");
				String idStr = currentFolderFile.getName().replaceAll(App.FILENAME_EXTENSION, "");
				if(idStr != null && isInteger(idStr)) {
					int id = Integer.parseInt(idStr);
					Wallpaper wallpaperDB = App.data.obtenerWallpaper(id);
					if(wallpaperDB != null) {
						wallpaper = wallpaperDB;
					}
					wallpaper.id = id;
				}
				
				wallpaper.archivoDirecto = currentFolderFile.getAbsolutePath();
				wallpaper.fileLastMod = currentFolderFile.lastModified();
				wallpapers.add(wallpaper);
			}
			else {
				getFolderWallpapers(currentFolderFile, wallpapers);
			}
		}
		
		return wallpapers;
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    // only got here if we didn't return false
	    return true;
	}
}