package com.gmi.wallcentralclientbase.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.gmi.wallcentralclientbase.App;
import com.gmi.wallcentralclientbase.domain.Wallpaper;

public class DownloadWallpaperTask extends AsyncTask<Wallpaper, Integer, Byte> {
	
	private DisplayMetrics displaymetrics;
	private View view;
	private Wallpaper wallpaper;
	private DownloadWallpaperIf listener;
	private File outputFile;
	
	public DownloadWallpaperTask(View view, DownloadWallpaperIf listener) {
		this.view = view;
		this.listener = listener;
	}
	
	@Override
	protected Byte doInBackground(Wallpaper... params) {
		
		byte status = -1;
		this.wallpaper = params[0];
		
		//chequeamos que la carpeta 
		//de salida exista
		App.WALLPAPERS_OUTPUT_FOLDER.mkdirs();
		
		//creamos el archivo de salida 
		//contenedor del wallpaper
		outputFile = new File(App.WALLPAPERS_OUTPUT_FOLDER.getAbsolutePath(), wallpaper.obtenerNombreArchivo());
		
		//take CPU lock to prevent CPU from going off if the user
		//presses the power button during download
		PowerManager pm = (PowerManager) view.getContext().getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
		wl.acquire();
		
		//get the window manager to 
		//get the display metrics 
		if(displaymetrics == null) {
			WindowManager windowManager = (WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE);
			displaymetrics = new DisplayMetrics();
			windowManager.getDefaultDisplay().getMetrics(displaymetrics);
		}
		
		InputStream input = null;
		HttpURLConnection connection = null;
		try {
			URL url = new URL(wallpaper.obtenerUrl(displaymetrics.widthPixels, displaymetrics.heightPixels));
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();

			// expect HTTP 200 OK, so we don't mistakenly save error report
			// instead of the file
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				outputFile.delete();
				status = DownloadWallpaperIf.STATUS_ERROR;
			}
			else {
				
				// this will be useful to display download percentage
				// might be -1: server did not report the length
				int fileLength = connection.getContentLength();

				// download the file
				input = connection.getInputStream();
				
				//creamos el flujo de salida al archivo
				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				byte data[] = new byte[4096];
				long total = 0;
				int count;
				while ((count = input.read(data)) != -1) {
					// allow cancelling with back button
					if (isCancelled()) {
						status = DownloadWallpaperIf.STATUS_CANCELLED;
					}
					total += count;
					// publishing the progress....
					if (fileLength > 0) // only if total length is known
						publishProgress((int) (total * 100 / fileLength));
					baos.write(data, 0, count);
				}
				
				//obtenemos la data de la imagen en memoria
				byte[] imageData = baos.toByteArray();
				FileOutputStream fos = new FileOutputStream(outputFile);
				fos.write(imageData);
				fos.flush();
				fos.close();
				
				//everything ok with the download
				status = DownloadWallpaperIf.STATUS_DOWNLOADED;
				
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			status = DownloadWallpaperIf.STATUS_ERROR;
		} 
		finally {
			wl.release();
			try {
				if (input != null)
					input.close();
				if (connection != null)
					connection.disconnect();
			} 
			catch (Exception ignored) {
			}
		}
		
		//solo si no dio error borro el archivo
		if(status != DownloadWallpaperIf.STATUS_DOWNLOADED) {
			outputFile.delete();
		}
		
		return status;
	}
	
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    
    @Override
    protected void onCancelled() {
    	super.onCancelled();
    	
    	//indicamos que finalizo el 
    	//download siendo cancelado
    	listener.downloadFinished(wallpaper, DownloadWallpaperIf.STATUS_CANCELLED, outputFile);
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false
//        activity.downloadProgressDialog.setIndeterminate(false);
//        activity.downloadProgressDialog.setMax(100);
//        activity.downloadProgressDialog.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Byte status) {
    	
    	//indicamos que finalizo el 
    	//download satisfactoriamente
    	listener.downloadFinished(wallpaper, status, outputFile);
    	
    }
}