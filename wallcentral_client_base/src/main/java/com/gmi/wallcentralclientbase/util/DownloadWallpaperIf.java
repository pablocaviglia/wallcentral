package com.gmi.wallcentralclientbase.util;

import com.gmi.wallcentralclientbase.domain.Wallpaper;


public interface DownloadWallpaperIf {
	
	public static final byte STATUS_DOWNLOADED = 0;
	public static final byte STATUS_ERROR = 1;
	public static final byte STATUS_CANCELLED = 2;
	
	public void downloadFinished(Wallpaper wallpaper, byte status, Object result);
	
}
