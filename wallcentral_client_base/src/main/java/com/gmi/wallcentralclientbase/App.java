package com.gmi.wallcentralclientbase;

import java.io.File;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.gmi.wallcentralclientbase.db.PersistenceHelper;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.L;

public abstract class App extends Application {
	
	//cargador de imagenes
	public static ImageLoader imageLoader = ImageLoader.getInstance();
	
    //opciones para el cargador de imagenes
	public static DisplayImageOptions options;
	
	//categoria base
	public static String CATEGORIA_BASE;
	
	//ads
	public static final int MAXIMO_WALLPAPERS_DESCARGADOS_SIN_CLIQUEAR_AD = 7;
	
	public static String GOOGLE_PLAY_APPLICATION_URL 				= "https://play.google.com/store/apps/details?id=";
	
	public static final String BUNDLE_SEARCH_KEYWORDS		 		= "searchKeywords";
	public static final String BUNDLE_DEFAULT_SEARCH_RESULT 		= "defaultSearchResult";
	public static final String BUNDLE_WALLPAPERS_PREVIO_REINICIO 	= "wallpapersPrevioReinicio";
	public static final String BUNDLE_WALLPAPER						= "wallpaper";
	
	public static String ADMOB_BANNER_ID = "ca-app-pub-9022829907836233/1110749705";
	public static String ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-9022829907836233/1133782504";
	
	public static final String FILENAME_EXTENSION = ".jpg";
	public static File WALLPAPERS_OUTPUT_FOLDER;
	
	//persistence
	public static PersistenceHelper data;
	
	//server url
	public static final String SERVER_BASE = "http://androidwallcentral.com";
	public static final String SERVER_APPLICATION_CONTEXT = SERVER_BASE + "/wallpaperCentral";
	
	public abstract void initialize();
	
	@Override
	public void onCreate() {
		
		super.onCreate();
		initialize();
		
		//completamos la url de esta
		//aplicacion en google play
		GOOGLE_PLAY_APPLICATION_URL += getApplicationContext().getPackageName();
		
		//inicializamos la persistencia
	    data = new PersistenceHelper(this.getApplicationContext());
	    
	    //indicamos la carpeta local
	    //donde van a ser guardados
	    //los wallpapers
	    String folderName = "WallCentral";
	    if(CATEGORIA_BASE != null && !CATEGORIA_BASE.trim().equals("")) {
	    	folderName += "/" + CATEGORIA_BASE;
	    }
	    
	    //creamos la carpeta de escritura
	    WALLPAPERS_OUTPUT_FOLDER = new File(Environment.getExternalStorageDirectory().getPath() + "/" + folderName + "/");
	    WALLPAPERS_OUTPUT_FOLDER.mkdirs();
	    
		//inicializamos el 
	    //cargador de imagenes
		initImageLoader(getApplicationContext());
		
	}

	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.FIFO)
				.build();
		// Initialize ImageLoader with configuration
		imageLoader.init(config);
		
		//opciones para desplegado de imagenes
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.ic_empty)
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.broken)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
		
		//disable imageloader logging
		L.disableLogging();
		
	}
}