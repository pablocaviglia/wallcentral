package com.gmi.wallcentralclientbase.listener;

public interface LogicFacadeListener {

	public byte OPERATION_ObtenerWallpapers = 1;
	public byte OPERATION_Login = 2;
	public byte OPERATION_Metadata = 3;
	
	public void operationFinished(Object result, byte operationType);
	
}
