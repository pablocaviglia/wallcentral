package com.gmi.wallcentralclientbase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.gmi.wallcentralclientbase.domain.ResultLogin;
import com.gmi.wallcentralclientbase.listener.LogicFacadeListener;
import com.gmi.wallcentralclientbase.logic.LogicFacade;

public class SplashScreenActivity extends Activity implements LogicFacadeListener {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        //significa que el usuario 
        //quiere finalizar la aplicacion
        if(getIntent().getBooleanExtra("EXIT", false)) {
            finish();
            return;
        }
        
        //set the content view
        setContentView(R.layout.splash);
        
        //ejecutamos el login que obtiene
        //los resultados de la primer busqueda
    	LogicFacade.login(this);

    }
    
    @Override
	public void operationFinished(Object result, byte operationType) {
		
    	if(operationType == LogicFacadeListener.OPERATION_Login) {
    		
    		//This method will be executed once the timer is over
	        //Start your app main activity
			Intent i = new Intent(SplashScreenActivity.this, MenuPrincipalActivity.class);
	        i.putExtra(App.BUNDLE_DEFAULT_SEARCH_RESULT, (ResultLogin)result);
	        startActivity(i);
			
	        //close this activity
	        finish();
		}
	}
}