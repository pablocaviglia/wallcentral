package com.gmi.wallcentralclientbase;

import java.io.File;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.gmi.wallcentralclientbase.domain.Wallpaper;
import com.gmi.wallcentralclientbase.ui.view.TouchImageView;

public class FullscreenImageActivity extends Activity {

	private TouchImageView touchImageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_fullscreen_image);
		touchImageView = (TouchImageView) findViewById(R.id.imgDisplay);
		touchImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FullscreenImageActivity.this.onBackPressed();
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		//obtenemos desde el bundle del
    	//que nos llama cual es el objeto
    	//wallpaper a hacer preview
    	Bundle extrasBundle = getIntent().getExtras();
       	Object wallpaperObj = extrasBundle.get(App.BUNDLE_WALLPAPER);
       	iniciarPreviewImagen(((Wallpaper)wallpaperObj));
		
	}
	
	public void iniciarPreviewImagen(Wallpaper wallpaper) {
		
		//si el archivo ya existe cargamos
		//la imagen en alta definicion, sino
		//solamente cargamos el thumb
		String urlImagen = null;
		
		if(wallpaper.obtenerArchivo().exists()) {
			urlImagen = "file://" + wallpaper.obtenerArchivo().getPath();
		}
		else {
			urlImagen = wallpaper.obtenerUrlThumb();
		}

		//cargamos la imagen
        App.imageLoader.displayImage(urlImagen, touchImageView, App.options);
        
        //refrescamos la vista y pedimos
        //request de la misma
        touchImageView.refreshDrawableState();
        
	}
}