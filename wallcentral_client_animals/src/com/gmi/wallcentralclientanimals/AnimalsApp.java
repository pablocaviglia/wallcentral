package com.gmi.wallcentralclientanimals;
import com.gmi.wallcentralclientbase.App;

public class AnimalsApp extends App {
	
	@Override
	public void initialize() {
		App.CATEGORIA_BASE = "animals";
		App.ADMOB_BANNER_ID = "ca-app-pub-6328885496949159/1639845827";
		App.ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-6328885496949159/3116579029";
	}
}