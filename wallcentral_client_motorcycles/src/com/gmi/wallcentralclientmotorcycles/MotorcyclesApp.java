package com.gmi.wallcentralclientmotorcycles;
import com.gmi.wallcentralclientbase.App;

public class MotorcyclesApp extends App {
	
	@Override
	public void initialize() {
		App.CATEGORIA_BASE = "motorcycles";
	}
}